'use strict'

const mongoose = require('mongoose')
const Env = use('Env')

// const server = '172.27.20.137'
const server = Env.get('MONGODB_HOST', 'localhost')
const username = Env.get('MONGODB_USER', 'admin')
const password = Env.get('MONGODB_PASSWORD', '')
const port = Env.get('MONGODB_PORT', 27017)
const database = Env.get('MONGODB_DATABASE', 'amipass')

module.exports = {
    connect: (db = null) => {
        let mongodb = (db == null) ? database : `${db}`.toUpperCase().replace(' ', '')
        return mongoose.connect(`mongodb://${server}/${mongodb}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            connectTimeoutMS: 1000,
            socketTimeoutMS: 1000,
            autoReconnect: true
        })
        .catch(error => {
            throw error
        })
    }
}