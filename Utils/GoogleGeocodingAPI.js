const Env = use('Env')
const KEY_API = Env.get('GEOCODING_KEY', '')
const UserLocation = use('App/Models/UserLocation');
const {Client} = require("@googlemaps/google-maps-services-js");

class Geocoding {
    async ReverseGeocoding(latitude, longitude, idusers) {
        return new Client({}).reverseGeocode({
            params: {
                key: KEY_API,
                latlng: {lat: latitude, lng: longitude},
                result_type: ["street_address"],
                location_type: ["ROOFTOP"]
            }
        }).then(async response => {
            const { results } = response.data
            const { formatted_address, address_components } = results[0]
            let addressComponents = {
                address: formatted_address
            }
            address_components.map(item => {
                item.types.filter(type => {
                    if (type == 'street_number') {
                        addressComponents["street_number"] = item.long_name
                    }
                    if (type == 'route') {
                        addressComponents["route"] = item.long_name
                    }
                    if (type == 'locality') {
                        addressComponents["locality"] = item.long_name
                    }
                    if (type == 'administrative_area_level_3') {
                        addressComponents["area_level_3"] = item.long_name
                    }
                    if (type == 'administrative_area_level_2') {
                        addressComponents["area_level_2"] = item.long_name
                    }
                    if (type == 'administrative_area_level_1') {
                        addressComponents["area_level_1"] = item.long_name
                    }
                    if (type == 'country') {
                        addressComponents["country"] = item.long_name
                    }
                })
            })
            return await UserLocation.create({
                idusers,
                ...addressComponents,
                latitude,
                longitude
            })
        })
    }

    async ReverseGeocodingCommune(latitude, longitude) {
        return new Client({}).reverseGeocode({
            params: {
                key: KEY_API,
                latlng: {lat: latitude, lng: longitude},
                result_type: ["street_address", "administrative_area_level_3", "establishment"],
                location_type: ["ROOFTOP"]
            }
        }).then(async response => {
            const { results } = response.data
            if(results.length > 0) {
                const { formatted_address, address_components } = results[0]
                let addressComponents = {
                    address: formatted_address
                }
                address_components.map(item => {
                    item.types.filter(type => {
                        if (type == 'administrative_area_level_3') {
                            addressComponents["commune"] = item.long_name //Comuna
                        }
                    })
                })
                return addressComponents
            } else {
                return null
            }
        })
    }
}

module.exports = {
    Geocoding
}