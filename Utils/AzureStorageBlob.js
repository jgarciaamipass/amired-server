const {BlobServiceClient, StorageSharedKeyCredential, newPipeline} = require("@azure/storage-blob");
const Env = use('Env');

class AzureStorageBlob {
    AzureStorageClient() {
        const account = Env.get('AZURE_ACCOUNT', 'assetsamipass');
        const accountKey = Env.get('AZURE_ACCOUNT_KEY', '');
        const sharedKeyCredential = new StorageSharedKeyCredential(account, accountKey);
        const pipeline = newPipeline(sharedKeyCredential);
        
        const blobServiceClient = new BlobServiceClient(
        `https://${account}.blob.core.windows.net`,
        pipeline
        );
        
        return blobServiceClient.getContainerClient('commerces');
    }
}

module.exports = {
    AzureStorageBlob
}