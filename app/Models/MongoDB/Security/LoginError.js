const mongoose = require('mongoose')

const schema = mongoose.Schema

const LoginErrorSchema = new schema({
    username: {
        type: String,
        index: true
    },
    password: {
        type: String
    },
    remember: {
        type: Boolean
    },
    ip: {
        type: String
    },
    headers: {
        type: String
    },
    create_at: {
        type: Date,
        default: Date.now,
        index: true
    }
})

module.exports = mongoose.model('login_error', LoginErrorSchema)