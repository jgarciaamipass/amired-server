const mongoose = require('mongoose')

const schema = mongoose.Schema

const DevicesUserSchema = new schema({
    latitude: {
        type: Number
    },
    longitude: {
        type: Number
    },
    username: {
        type: String
    },
    employer: {
        type: String
    },
    battery: {
        type: String
    },
    brand: {
        type: String
    },
    carrier: {
        type: String
    },
    device: {
        type: String
    },
    deviceName: {
        type: String
    },
    hardware: {
        type: String
    },
    ip: {
        type: String
    },
    mac: {
        type: String
    },
    manufacturer: {
        type: String
    },
    model: {
        type: String
    },
    phoneNumber: {
        type: String
    },
    product: {
        type: String
    },
    so: {
        type: String
    },
    osName: {
        type: String
    },
    modelId: {
        type: String
    },
    uniqueID: {
        type: String
    },
    location: {
        type: Boolean
    },
    locationProvider: {
        type: String
    },
    tokenFirebase: {
        type: String
    },
    create_at: {
        type: Date,
        default: Date.now,
        index: true
    }
})

module.exports = mongoose.model('device', DevicesUserSchema)