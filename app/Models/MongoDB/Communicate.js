const mongoose = require('mongoose')

const schema = mongoose.Schema

const CommunicateSchema = new schema({
    title: String,
    description: String,
    actived: {
        type: Boolean,
        index: true
    },
    path: String,
    create_at: {
        type: Date,
        default: Date.now,
        index: true
    }
})

module.exports = mongoose.model('communicate', CommunicateSchema)