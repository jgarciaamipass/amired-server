const mongoose = require('mongoose')

const schema = mongoose.Schema

const CommunicationSeenSchema = new schema({
    communicateId: {
        type: String,
        index: true,
        unique: true
    },
    readed: {
        type: Boolean,
        default: false,
        index: true
    },
    create_at: {
        type: Date,
        default: Date.now,
        index: true
    }
})

module.exports = mongoose.model('communication_seen', CommunicationSeenSchema)