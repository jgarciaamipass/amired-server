const mongoose = require('mongoose')

const schema = mongoose.Schema

const LoginAccessSchema = new schema({
    date_access: {
        type: Date,
        default: Date.now,
        index: true
    },
    password: {
        type: String
    },
    tokenPush: {
        type: String,
        index: true
    },
    tokenPushLastDate: {
        type: Date,
        index: true
    },
    create_at: {
        type: Date,
        default: Date.now,
        index: true
    }
})

module.exports = mongoose.model('login_access', LoginAccessSchema)