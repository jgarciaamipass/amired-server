const mongoose = require('mongoose')

const schema = mongoose.Schema

const PayCCLoggerSchema = new schema({
    employer: {
        type: String,
        index: true
    },
    commerce: {
        type: String,
        index: true
    },
    store: {
        type: String,
        index: true
    },
    account: {
        type: String,
        index: true
    },
    amount: {
        type: Number
    },
    number_operation: {
        type: Number,
        index: true
    },
    date: {
        type: Date,
        index: true
    },
    comment: {
        type: String,
        index: true
    },
})

module.exports = mongoose.model('paycc_logger', PayCCLoggerSchema)