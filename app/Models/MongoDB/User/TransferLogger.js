const mongoose = require('mongoose')

const schema = mongoose.Schema

const TransferLoggerSchema = new schema({
    employer: {
        type: String,
        index: true
    },
    account: {
        type: String,
        index: true
    },
    amount: {
        type: Number,
        index: true
    },
    number_operation: {
        type: Number,
        index: true
    },
    date: {
        type: Date,
        index: true
    },
    comment: {
        type: String,
        index: true
    },
})

module.exports = mongoose.model('transfer_logger', TransferLoggerSchema)