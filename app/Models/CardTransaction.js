'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CardTransaction extends Model {
  static get table() {
    return 'cards_transactions'
  }
  static get primaryKey() {
    return 'idcards_transactions'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = CardTransaction
