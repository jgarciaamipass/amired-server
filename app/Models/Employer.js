'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Employer extends Model {
  static get table() {
    return 'employer'
  }
  static get primaryKey() {
    return 'idemployer'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = Employer
