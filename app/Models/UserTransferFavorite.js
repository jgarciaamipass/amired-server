'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserTransferFavorite extends Model {
  static get table() {
    return 'users_transfer_favorite'
  }
  static get primaryKey() {
    return 'idusers_transfer_favorite'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = UserTransferFavorite
