'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserPaySmartFavorite extends Model {
  static get table() {
    return 'user_paysmart_favorite'
  }
  static get primaryKey() {
    return 'iduser_paysmart_favorite'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = UserPaySmartFavorite
