'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Commerce extends Model {
  static get table() {
    return 'commerce'
  }
  static get primaryKey() {
    return 'idcommerce'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
  stores() {
    return this.hasMany('App/Models/CommereStore','idcommerce', 'idcommerce')
  }
}

module.exports = Commerce
