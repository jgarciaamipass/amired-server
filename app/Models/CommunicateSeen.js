'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CommunicateSeen extends Model {
  static get table() {
    return 'communicate_seen'
  }
  static get primaryKey() {
    return 'idcommunicate_seen'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = CommunicateSeen
