'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class BranchOffice extends Model {
  static get table() {
    return 'branch_office'
  }
  static get primaryKey() {
    return 'idbranch_office'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = BranchOffice
