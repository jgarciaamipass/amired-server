'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CommerceStore extends Model {
  static get table() {
    return 'commerce_store'
  }
  static get primaryKey() {
    return 'idcommerce_store'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
  commerce() {
    return this.belongsTo('App/Models/Commerce', 'idcommerce', 'idcommerce')
  }
}

module.exports = CommerceStore
