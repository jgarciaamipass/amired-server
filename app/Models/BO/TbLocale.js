'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbLocale extends Model {
  static get connection() {
    return 'mssql'
  }
  static get table() {
    return 'TbLocales'
  }
  static get primaryKey() {
    return 'sEstablecimiento'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
  static get hidden() {
    return [
      'sPagoPOS',
      'sContactoTipo',
      'sContactoNombre',
      'sContactoEmail',
      'sContactoTelefono',
      'sPosMulticaja',
      'gGeoUbicacion',
      'dFechaSolicitudHabilitacion',
      'sClasificacion',
      'dActivacion',
      'bCobroAmipass',
      'dFechaArchivo',
      'sLocalSW',
      'sEmailFactura',
      'sNumeroCuenta',
      'sBanco',
      'sRutTitularCuenta',
      'sNombreTitularCuenta',
      'sTelefonoLocal',
      'sContactoMovil',
      'bDelivery',
      'bAmipassPay',
      'bLogo',
      'bPruebaPOS',
      'bStopper',
      'bCapacitacion',
      'bIntervencionUrbana',
      'bMenuAmipass',
      'bAmipesos',
      'sCostoPOS',
      'sPeriodoCobro',
      'dFechaSolicitudDesvinculacion',
      'bAmipassPayPc',
      'bAmipassPayTabletWifi',
      'bAmipassPayTabletChip',
      'bActivarMapa',
      'bPosEthernet',
      'bDireccionRevisada',
      'bPosGrps',
      'bSmartphone',
      'sNChip',
      'sNTablet',
      'sResolucionSanitaria',
      'sEmailTransacciones',
      'bH2H',
      'bTablet3G',
      'bPortaTablet',
      'bPagoSmartphone',
      'sLocalH2H',
      'bJUNAEB',
      'bMenuJUNAEB',
      'nMenuJUNAEBValor',
      'sClasificacionJUNAEB',
      'sLocalPay',
      'bAppPAY',
      'bSmartphoneIntegrado',
      'bCasino',
      'dFechaEstadoActivo',
      'bLocalFisico',
      'bLocaldigital',
      'bResolucionAplica',
      'bResolucionTiene',
      'sSoftwareCaja',
      'bRepartoRetiro',
      'bRepartoDelivery',
      'bRepartoFisico',
    ]
  }
  establecimiento() {
    return this.hasOne('App/Models/BO/TbEstablecimiento', 'sEstablecimiento', 'sEstablecimiento')
  }
}

module.exports = TbLocale
