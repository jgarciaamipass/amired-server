'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbUsuario extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table(){
        return 'TbUsuarios'
    }
    static get primaryKey() {
        return 'iUsuario'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
}

module.exports = TbUsuario
