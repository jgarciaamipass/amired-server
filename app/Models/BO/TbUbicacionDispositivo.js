'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbUbicacionDispositivo extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table() {
        return 'TbUbicacionDispositivo'
    }
    static get primaryKey() {
        return 'nUbicacion'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
}

module.exports = TbUbicacionDispositivo
