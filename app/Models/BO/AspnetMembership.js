'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AspnetMembership extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table(){
        return 'aspnet_Membership'
    }
    static get primaryKey() {
        return 'UserId'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
    static get hidden() {
        return [
            'ApplicationId',
            'UserId',
            'LoweredEmail',
            'IsApproved',
            'IsLockedOut',
            'Comment', 
            'FailedPasswordAnswerAttemptWindowStart',
            'FailedPasswordAnswerAttemptCount',
            'FailedPasswordAttemptWindowStart',
            'FailedPasswordAttemptCount',
            'LastLockoutDate',
            'LastPasswordChangedDate',
            'LastLoginDate',
            'CreateDate',
            'PasswordAnswer',
            'PasswordQuestion',
            'MobilePIN',
            'PasswordSalt',
            'PasswordFormat'
        ]
    }
}

module.exports = AspnetMembership
