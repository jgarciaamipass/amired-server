'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbTarjeta extends Model {
  static get connection() {
    return 'mssql'
  }
  static get table() {
    return 'TbTarjetas'
  }
  static get primaryKey() {
    return 'sTarjeta'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = TbTarjeta
