'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbEmpresa extends Model {
  static get connection() {
    return 'mssql'
  }
  static get table() {
    return 'TbEmpresas'
  }
  static get primaryKey() {
    return 'sEmpresa'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = TbEmpresa
