'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbEnvioPush extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table() {
        return 'TbEnvioPush'
    }
    static get primaryKey() {
        return 'ID'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
}

module.exports = TbEnvioPush
