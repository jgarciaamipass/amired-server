'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbComunas extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table(){
        return 'TbComunas'
    }
    static get primaryKey() {
        return 'sComuna'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
}

module.exports = TbComunas
