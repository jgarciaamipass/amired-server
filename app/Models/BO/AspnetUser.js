'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AspnetUser extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table(){
        return 'aspnet_Users'
    }
    static get primaryKey() {
        return 'UserId'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
    static get hidden() {
        return [
            'ApplicationId',
            'LastActivityDate', 
            'IsAnonymous',
            'MobileAlias',
            'LoweredUserName'
        ]
    }
}

module.exports = AspnetUser
