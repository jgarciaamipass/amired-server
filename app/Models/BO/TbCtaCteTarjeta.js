'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbCtaCteTarjeta extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table(){
        return 'TbCtaCteTarjetas'
    }
    static get primaryKey() {
        return 'iCorrelativo'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
    static get dates() {
        return super.dates.concat(['dMovimiento'])
    }
    // static formatDates(field, value) {
    //     if(field === 'dMovimiento'){
    //         return value.format('YYYY-MM-DD HH:mm:ss');
    //     }
    //     return super.formatDates(field, value);
    // }
}

module.exports = TbCtaCteTarjeta
