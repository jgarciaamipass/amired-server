'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbPromocione extends Model {
  static get connection() {
    return 'mssql'
  }
  static get table() {
    return 'TbPromocionesNuevas'
  }
  static get primaryKey() {
    return 'idPromocion'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
  static get dates() {
    return super.dates.concat(['date_init', 'date_end'])
  }
}

module.exports = TbPromocione
