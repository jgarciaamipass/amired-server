'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbEstablecimiento extends Model {
  static get connection() {
    return 'mssql'
  }
  static get table() {
    return 'TbEstablecimientos'
  }
  static get primaryKey() {
    return 'sEstablecimiento'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
  locales() {
    return this.hasMany('App/Models/BO/TbLocale', 'sEstablecimiento', 'sEstablecimiento')
  }
}

module.exports = TbEstablecimiento
