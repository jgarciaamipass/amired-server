'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbEmpleado extends Model {
  static get connection() {
    return 'mssql'
  }
  static get table() {
    return 'TbEmpleados'
  }
  static get primaryKey() {
    return ['sEmpleado', 'sEmpresa']
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
  static get dates() {
    return super.dates.concat(['dNacimiento'])
  }
}

module.exports = TbEmpleado
