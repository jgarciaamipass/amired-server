'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbUsuariosPay extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table(){
        return 'TbUsuariosPay'
    }
    static get primaryKey() {
        return 'sUsuario'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
}

module.exports = TbUsuariosPay
