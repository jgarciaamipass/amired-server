'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbClaveTemporal extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table(){
        return 'TbClaveTemporal'
    }
    static get primaryKey() {
        return 'nClave'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
    static get dates() {
        return super.dates.concat([
            'dVencimiento'
        ])
    }
    // static castDates(field, value) {
    //     if(field == 'dVencimiento') {
    //         return `${value.toISOString().replace(/([^T]+)T([^\.]+).*/g, '$1 $2')}`
    //     }
    //     return super.formatDates(field, value)
    // }
    // static castDates(field, value) {
    //     if(field == 'dVencimiento') {
    //         return value.toNow()
    //     }
    //     return super.formatDates(field, value)
    // }
    // static formatDates(field, value) {
    //     if (field === 'dVencimiento') {
    //         return value.format('YYYY-MM-DD')
    //     }
    //     return super.formatDates(field, value)
    // }
}

module.exports = TbClaveTemporal
