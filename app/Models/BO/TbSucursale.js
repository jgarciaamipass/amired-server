'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbSucursale extends Model {
  static get connection() {
    return 'mssql'
  }
  static get table() {
    return 'TbSucursales'
  }
  static get primaryKey() {
    return ['sEmpresa', 'sSucursal']
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = TbSucursale
