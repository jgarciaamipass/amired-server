'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TbGruposEmpresa extends Model {
    static get connection() {
        return 'mssql'
    }
    static get table() {
    return 'TbGruposEmpresa'
    }
    static get primaryKey() {
    return 'idGrupo'
    }
    static get createdAtColumn() {
    return null
    }
    static get updatedAtColumn() {
    return null
    }
}

module.exports = TbGruposEmpresa
