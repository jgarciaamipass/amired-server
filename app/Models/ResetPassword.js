'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ResetPassword extends Model {
    static get table() {
        return 'reset_password'
    }
    static get primaryKey() {
        return 'idreset_password'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
}
}

module.exports = ResetPassword
