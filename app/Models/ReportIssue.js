'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ReportIssue extends Model {
    static get table() {
        return 'report_issue'
    }
    static get primaryKey() {
        return 'idreport_issue'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
}
}

module.exports = ReportIssue
