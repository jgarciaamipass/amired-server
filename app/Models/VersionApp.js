'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VersionApp extends Model {
}

module.exports = VersionApp
