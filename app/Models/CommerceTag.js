'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CommerceTag extends Model {
  static get table() {
    return 'commerce_tags'
  }
  static get primaryKey() {
    return 'idcommerce_tags'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
  tag() {
    return this.belongsTo('App/Models/Tag', 'idtags', 'idtags')
  }
}

module.exports = CommerceTag
