'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserTerm extends Model {
    static get table() {
        return 'user_terms'
    }
    static get primaryKey() {
        return 'id'
    }
    static get createdAtColumn() {
        return 'created_at'
    }
    static get updatedAtColumn() {
        return 'updated_at'
    }

    // terms() {
    //     return this.hasMany('App/Models/Token', 'idusers', 'user_id')
    // }
}

module.exports = UserTerm
