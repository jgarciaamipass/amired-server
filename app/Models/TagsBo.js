'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TagsBo extends Model {
    static get table() {
        return 'tags_bo'
    }
    static get primaryKey() {
        return 'idtags_bo'
    }
    static get createdAtColumn() {
        return null
    }
    static get updatedAtColumn() {
        return null
    }
}

module.exports = TagsBo
