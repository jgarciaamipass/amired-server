'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserTransfer extends Model {
  static get table() {
    return 'users_transfers'
  }
  static get primaryKey() {
    return 'idusers_transfers'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = UserTransfer
