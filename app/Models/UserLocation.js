'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserLocation extends Model {
    static get table() {
        return 'users_location'
    }
    static get primaryKey() {
        return 'idusers_location'
    }
    static get updatedAtColumn() {
        return null
    }
}

module.exports = UserLocation
