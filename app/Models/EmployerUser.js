'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class EmployerUser extends Model {
  static get table() {
    return 'employer_users'
  }
  static get primaryKey() {
    return 'idemployer_users'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = EmployerUser
