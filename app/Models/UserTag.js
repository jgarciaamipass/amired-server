'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserTag extends Model {
  static get table() {
    return 'users_tags'
  }
  static get primaryKey() {
    return 'idusers_tags'
  }
  static get createdAtColumn() {
    return null
  }
  static get updatedAtColumn() {
    return null
  }
}

module.exports = UserTag
