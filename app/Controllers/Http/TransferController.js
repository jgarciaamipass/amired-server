'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with transfers
 */

const GE = require('@adonisjs/generic-exceptions')
const Env = use('Env')
const Database = use('Database')
const axios = require('axios')
const UserTransferFavorite = use('App/Models/UserTransferFavorite')
const BOCards = use('App/Models/BO/TbTarjeta');
const Transaction = use('App/Models/Transaction')

class TransferController {
  /**
   * Show a list of all transfers.
   * GET transfers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response }) {
    try {
      const { card } = request.body

      const infoCard = await BOCards.query().select([
        'sEmpleado as employee',
        'sEmpresa as employer'
      ])
        .where('sTarjeta', card)
        .first()

      const result = await Database.connection('mssql').raw(
        `
        select top 10
              max(trans.dFecha) [last],
              emple.sNombre + ' ' + emple.sApellidoPaterno [name],
              emple.sEmpleado [code],
              trans.sTarjetaDestino [card]
        from TbLogTraspasoSaldos trans
            inner join TbTarjetas tar on trans.sTarjetaDestino = tar.sTarjeta and tar.sEstado in ('AC')
            inner join TbEmpleados emple on tar.sEmpresa = emple.sEmpresa and tar.sEmpleado = emple.sEmpleado
        where trans.sTarjetaOrigen in (select sTarjeta from TbTarjetas where sEmpleado = :employee and sEmpresa= :employer )
        group by trans.sTarjetaDestino, emple.sEmpleado, emple.sNombre,emple.sApellidoPaterno
        order by 1 desc
        `, {
        employee: infoCard.employee,
        employer: infoCard.employer
      })

      return {
        message: null,
        data: result
      }
    } catch (error) {
      console.log('index Transfer')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new transfer.
   * GET transfers/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({ request, response }) {
  }

  /**
   * Create/save a new transfer.
   * POST transfers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    try {
      const { employee, employer, account, pin, amount, frecuent, comment } = request.body

      const result = await axios({
        url: `${Env.get('API_V1')}/Transferencia`,
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({
          sEmpresa: employer,
          sEmpleado: employee,
          sClave: pin,
          nMonto: amount,
          sDestino: account
        })
      }).then(async (data) => {
        if (data.status === 200) {
          const { idTraspaso, oOrigen, oDestino, nMonto } = data.data
          let date = new Date(Date.now())

          date.setHours(date.getHours() - 4);

          await Transaction.create({
            account: oDestino.sTarjeta,
            accredit: 0,
            date: date,
            comment,
            debit: nMonto,
            description: '',
            number_operation: idTraspaso,
            type_operation: 'TRN',
            voided: false,
          })

          return {
            account: oDestino.sTarjeta,
            accredit: 0,
            comment,
            date: date,
            debit: Number(nMonto) * -1,
            description: "",
            from: `${oDestino.sNombre} ${oDestino.sApellidoPaterno} ${oDestino.sApellidoMaterno}`,
            idtransaction: idTraspaso,
            number_operation: idTraspaso,
            row: 0,
            type_operation: 'TRN',
            voided: 0,
          }
        }
        throw new GE.HttpException('Por favor intente más tarde\r(COD TRAB500)', 500)
      }).catch(error => {
        console.log('Transferencia benlar en Transfer')
        console.log(error)
        if (error.response.status == 406) {
          throw new GE.HttpException('Por favor intente nuevamente\r(COD TRAB403)', 403)
        }
        throw new GE.HttpException('Por favor intente más tarde\r(COD TRAB500)', 500)
      })
      response.status(201).send(result)
    } catch (error) {
      console.log('store Transfer')
      console.log(error)
      throw error
    }
  }

  /**
   * Display a single transfer.
   * GET transfers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {
  }

  /**
   * Render a form to update an existing transfer.
   * GET transfers/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({ params, request, response }) {
  }

  /**
   * Update transfer details.
   * PUT or PATCH transfers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a transfer with id.
   * DELETE transfers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = TransferController
