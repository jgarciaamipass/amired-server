'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with cards
 */

const GE = require('@adonisjs/generic-exceptions')
const Card = use('App/Models/Card')
const BOCard = use('App/Models/BO/TbTarjeta')
const Employer = use('App/Models/Employer');
const BOTbCtaCteTarjeta = use('App/Models/BO/TbCtaCteTarjeta')
const Env = use('Env')
const axios = require('axios');

class CardController {
  /**
   * Show a list of all cards.
   * GET cards
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response }) {
    const { page, limit } = request.body;
    const card = await Card.all();
    return {
      message: null,
      data: card
    }
  }

  /**
   * Render a form to be used for creating a new card.
   * GET cards/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create ({ request, response }) {
  }

  /**
   * Create/save a new card.
   * POST cards
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single card.
   * GET cards/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response, auth }) {
    try {
      const { id } = params
      const boCard = await BOCard.query()
      .select([
        'TbTarjetas.sTarjeta as code',
        'sTipoTarjeta as type',
        'sEmpresa as employer',
        'sEmpleado as employee',
        'dEmision as emit',
        'dVencimiento as expired',
        'sClave as pin',
        'sEstado as status',
        'sExpiracion as expire',
        'sCVC as cvc'
      ])
      .where('sTarjeta', id)
      .first()

      if(!boCard) throw new GE.HttpException('Tuvimos problemas configurando el usuario. Por favor comunicate a hola@amipass.com para solucionarlo a la brevedad\r(COD TAA404)', 404)

      const boTbCtaCteTarjeta = await BOTbCtaCteTarjeta
      .query()
      .leftJoin('TbTarjetas', 'TbCtaCteTarjetas.sTarjeta', 'TbTarjetas.sTarjeta')
      .where({
          'sEmpleado': boCard.employee,
          'sEmpresa': boCard.employer
      })
      .sum('nMonto as balance')
      .first()
      
      // const card = await Card.findOrCreate(
      //   { code: id },
      //   {
      //     code: boCard.card,
      //     idemployer: await Employer.findBy('rut', boCard.employer).first().id,
      //     idusers: auth.getUser().username,
      //     date: boCard.emit,
      //     expired: boCard.expired,
      //     type_card: boCard.type,
      //     state: boCard.state,
      //     password: boCard.pin,
      //     cvc: boCard.cvc,
      //     balance: 0
      //   }
      // )
      const {
        code,
        type,
        employer,
        employee,
        emit,
        expired,
        pin,
        status,
        expire,
        cvc
      } = boCard
      return {
        message: null,
        data: {
          code,
          type,
          employer,
          employee,
          emit,
          expired,
          pin,
          status,
          expire,
          cvc,
          balance: boTbCtaCteTarjeta.balance
        }
      }
    } catch (error) {
      console.log('show Card')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to update an existing card.
   * GET cards/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit ({ params, request, response }) {
  }

  /**
   * Update card details.
   * PUT or PATCH cards/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  async block ({ request, response }) {
    try {
      const { card, replacement } = request.body
      const result = await axios({
          url: `${Env.get('API_V1')}/BloqueoTarjeta`,
          method: 'post',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          data: JSON.stringify({
              sTarjeta: card,
              bReposicion: replacement
          })
      }).then((data) => { 
          const { bBloqueada } = data.data
          return bBloqueada
      })
      .catch((error) => {
        console.log('Bloqueo de tarjeta benlar Card')
        console.log(error)
        if(error.response.status == 406) {
          throw new GE.HttpException('No fue posible bloquear tu tarjeta, favor intenta más tarde\r(COD BLTB404)', 404)
        }
        throw new GE.HttpException('No fue posible bloquear tu tarjeta, favor intenta más tarde\r(COD BLTB500)', 500)
      })
      return {
          message: null,
          data: {
              blocked: result
          }
      }
    } catch (error) {
      console.log('block Card')
      console.log(error)
      throw error
    }
  }

  async reactive ({ request, response }) {
    try {
      const { card } = request.body
      const result = await axios({
          url: `${Env.get('API_V1')}/ReactivarTarjeta`,
          method: 'post',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          data: JSON.stringify({
              sTarjeta: card
          })
      }).then((data) => { 
          const { bActivada } = data.data
          return bActivada
      })
      .catch((error) => {
        console.log('Reactivar tarjeta benlar Card')
        console.log(error)
        if(error.response.status == 406) {
          throw new GE.HttpException('No fue posible bloquear tu tarjeta, favor intenta más tarde\r(COD RTRB404)', 404)
        }
        throw new GE.HttpException('No fue posible bloquear tu tarjeta, favor intenta más tarde\r(COD RTRB500)', 500)
      })
      return {
          message: null,
          data: {
              reactived: result
          }
      }
    } catch (error) {
      console.log('reactive Card')
      console.log(error)
      throw error
    }
  }

  /**
   * Delete a card with id.
   * DELETE cards/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = CardController
