'use strict'

const GE = require('@adonisjs/generic-exceptions')
const Database = use('Database')
const BOEmployee = use('App/Models/BO/TbEmpleado');
const BOEmployer = use('App/Models/BO/TbEmpresa');
const BOCards = use('App/Models/BO/TbTarjeta');
const BOCtaCteTarjeta = use('App/Models/BO/TbCtaCteTarjeta');
const BOTbCtaCteTarjeta = use('App/Models/BO/TbCtaCteTarjeta')
const BOUser = use('App/Models/BO/TbUsuario');
const Profile = use('App/Models/Profile');
const BORole = use('App/Models/BO/TbGruposEmpresa');

class HomeController {
    async index({ request, response, auth }) {
        try {
            let { user } = request.body
            let { username, idusers } = auth.user
            user = `${user}`.toUpperCase()
            username = `${username}`.toUpperCase()
            if(user !== username) throw new GE.HttpException('Tuvimos problemas configurando el usuario.\rPor favor comunicate a hola@amipass.com para solucionarlo a la brevedad\r(COD HA403)', 403)

            //consulta del usuario en BO
            const accounts = await BOUser
                .query()
                .select([
                    'iUsuario as id',
                    'sUsuario as username',
                    'emple.sNombre as firstname',
                    'emple.sApellidoPaterno as lastname',
                    'emple.sAPellidoMaterno as lastname2',
                    'emple.sRUT as rut',
                    'emple.sEMail as email',
                    'emple.sTelefonoMovil as mobile',
                    'emple.dNacimiento as birthday',
                    'TbUsuarios.sEmpresa as employer',
                    'empre.sRazonSocial as employerName',
                    'empre.sDescripcion as commercialName',
                    'emple.idGrupo as role'
                ])
                .leftJoin('TbEmpresas as empre', 'TbUsuarios.sEmpresa', 'empre.sEmpresa')
                .leftJoin('TbEmpleados as emple', {
                    'emple.sEmpleado': 'sUsuario',
                    'emple.sEmpresa': 'empre.sEmpresa',
                })
                .where({
                    sUsuario: username,
                    'sEstablecimiento': null
                })
                .orderBy('dActualizacion', 'desc')
                .fetch();
            
            //Recorrido de los usuarios
            const boAccounts = await accounts.rows.map( async (item) => {
                const boCards = await BOCards
                .query()
                .select([
                    'sTarjeta as card',
                    'sEstado as status'
                ])
                .where({
                    'sEmpleado': item.username,
                    'sEmpresa': item.employer
                })
                // .whereIn('sEstado', ['AC', 'SU', 'PS', 'BL'])
                .orderBy('dFechaSolicitud', 'desc')
                // .orderBy('nSolicitud', 'desc')
                .first()

                if(!boCards) throw new GE.HttpException('Tuvimos problemas configurando el usuario. Por favor comunicate a hola@amipass.com para solucionarlo a la brevedad\r(COD HTAA404)', 404)
                
                item['card'] = boCards.card
                item['status'] = (boCards.status !== 'AC') ? 'BL' : 'AC'
                
                const balance = await BOTbCtaCteTarjeta
                .query()
                .leftJoin('TbTarjetas', 'TbCtaCteTarjetas.sTarjeta', 'TbTarjetas.sTarjeta')
                .where({
                    'sEmpleado': item.username,
                    'sEmpresa': item.employer
                })
                .sum('nMonto as balance')
                .first()
                
                item['balance'] = (balance.balance == null) ? 0 : balance.balance
                
                const role = await BORole
                    .query()
                    .select([
                        'sObservacion as description',
                        'bEstadoCuenta as account',
                        'bPagoSmartphone as paySmart',
                        'bTransferenciaSaldo as transfer',
                        'bBuscadorLocales as finder',
                        'bClaveDinamica as dynamicKey',
                        'bAmiclub as promotion',
                        'bPagoCasino as dinning',
                        'bSaldo as balance'
                    ])
                    .where('idGrupo', item.role)
                    .first()

                if(!role) throw new GE.HttpException('Tuvimos problemas configurando el usuario. Por favor comunicate a hola@amipass.com para solucionarlo a la brevedad\r(COD HROLA404)', 404)
                
                item['role'] = role
                
                const profile = await Profile.findOrCreate({
                    username: `${username}`.toUpperCase(),
                    employer: item.employer
                    }, 
                    {
                        idusers,
                        username: `${username}`.toUpperCase(),
                        employer: item.employer
                    }
                )
                item['avatar'] = profile.avatar

                return await item
            })
            return {
                message: null,
                data: await Promise.all(boAccounts)
            }
        } catch (error) {
            console.log('index Home')
            console.log(error)
            throw error
        }
    }
}

module.exports = HomeController
