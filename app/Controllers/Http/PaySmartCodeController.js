'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with commerce
 */

const GE = require('@adonisjs/generic-exceptions')
const Env = use('Env')
const Database = use('Database')
const Commerce = use('App/Models/Commerce');
const CommerceStore = use('App/Models/CommerceStore')
const BOCommerce = use('App/Models/BO/TbEstablecimiento')
const BOCommerceStore = use('App/Models/BO/TbLocale')
const BOCards = use('App/Models/BO/TbTarjeta');
const BOUserPay = use('App/Models/BO/TbUsuariosPay');
const Transaction = use('App/Models/Transaction')
const axios = require('axios')

class PaySmartCodeController {
  /**
   * Show a list of all commerce.
   * GET commerce
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response, params }) {
    try {
        const { card } = request.body

        const infoCard = await BOCards.query().select([
          'sEmpleado as employee',
          'sEmpresa as employer'
        ])
        .where('sTarjeta', card)
        .first()

        const result = await Database.connection('mssql').raw(
        `
        SELECT top 10
            max(tra.dTransaccion) lastdate,
            tra.sEstablecimiento commerce,
            tra.sLocal store,
            loc.sDescripcion name,
            loc.sDireccion address,
            upay.sUsuario userPay
        from TbTransacciones tra
            inner join TbLocales loc on tra.sEstablecimiento = loc.sEstablecimiento and tra.sLocal = loc.sLocal
            inner join TbUsuariosPay upay on upay.sEstablecimiento = loc.sEstablecimiento and upay.sLocal = loc.sLocal
        where tra.sTarjeta in (select sTarjeta from TbTarjetas where sEmpleado = :employee and sEmpresa= :employer )
        group by tra.sEstablecimiento, tra.sLocal, loc.sDescripcion, loc.sDireccion, upay.sUsuario
        order by 1 desc
        `, {
          employee: infoCard.employee,
          employer: infoCard.employer
        })

        return {
            message: null,
            data: result
        }
    } catch (error) {
      console.log('index Pay Smart Code')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new commerce.
   * GET commerce/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({
    request,
    response
  }) {}

  /**
   * Create/save a new commerce.
   * POST commerce
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const { employee, employer, pin, commerce, store, amount, comment } = request.body
    try {
      const result = await axios({
        url: `${Env.get('API_V1')}/PagoSmart`,
        method: 'post',
        headers: {
				  'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({
          sEmpleado: employee,
          sEmpresa: employer,
          sClave: pin,
          sEstablecimiento: commerce,
          sLocal: store,
          nMonto: amount
        })
      }).then(async (data) => {
        if(data.status === 200)
        {
          const boUserPay = await BOUserPay.query()
          .select([
            'sUsuario as userpay'
          ])
          .where({
            'sEstablecimiento': commerce,
            'sLocal': store
          })
          .first()

          const { cDatosTX, cDatosEmpleado, cDatosComercio } = data.data

          console.log(data.data)

          let date = new Date(Date.now())

          date.setHours(date.getHours() - 3);
          
          await Transaction.create({
            number_operation: cDatosTX.idTransaccion,
            type_operation: 'PAY',
            date: date,
            voided: false,
            description: '',
            debit: cDatosTX.nMonto * -1,
            accredit: 0,
            // account: `${cDatosComercio.sEstablecimiento}${cDatosComercio.sLocal}`,
            account: boUserPay.userpay,
            comment,
          })

          return {
            // account: `${cDatosComercio.sEstablecimiento}${cDatosComercio.sLocal}`,
            account: boUserPay.userpay,
            accredit: 0,
            date: date,
            debit: cDatosTX.nMonto * -1,
            description: '',
            from: cDatosComercio.sNombreLocal,
            idtransaction: cDatosTX.idTransaccion,
            number_operation: cDatosTX.idTransaccion,
            row: 0,
            type_operation: 'PAY',
            voided: 0,
            comment
          }
        }
        throw new GE.HttpException('Por favor intente más tarde\r(COD PCCB500)', 500)
      }).catch(error => {
        console.log('PagoSmart benlar en Pay Smart Code')
        console.log(error)
        if(error.response.status == 406) {
          throw new GE.HttpException('Error en la transferencia o Saldo insuficiente \r(COD PCCB403)', 403)
        }
        throw new GE.HttpException('Por favor intente más tarde\r(COD PCCB500)', 500)
      })
      response.status(201).send(result)
    } catch (error) {
      console.log('show Pay Smart Code')
      console.log(error)
      throw error
    }
  }

  /**
   * Display a single commerce.
   * GET commerce/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {
    
  }

  /**
   * Render a form to update an existing commerce.
   * GET commerce/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({
    params,
    request,
    response
  }) {}

  /**
   * Update commerce details.
   * PUT or PATCH commerce/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({
    params,
    request,
    response
  }) {}

  /**
   * Delete a commerce with id.
   * DELETE commerce/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({
    params,
    request,
    response
  }) {}
}

module.exports = PaySmartCodeController
