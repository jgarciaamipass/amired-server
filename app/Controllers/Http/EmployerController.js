'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with employers
 */

const GE = require('@adonisjs/generic-exceptions')
const Employer = use('App/Models/Employer')
const BOEmployer = use('App/Models/BO/TbEmpresa')

class EmployerController {
  /**
   * Show a list of all employers.
   * GET employers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response }) {
    const employer = await Employer.all()
    return {
      message: null,
      data: employer
    }
  }

  /**
   * Render a form to be used for creating a new employer.
   * GET employers/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create ({ request, response }) {
  }

  /**
   * Create/save a new employer.
   * POST employers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single employer.
   * GET employers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
    try {
      const { id } = params
      const boEmployer = await BOEmployer.query()
      .select([
        'sRazonSocial as name',
        'sRUT as rut',
        'sDescripcion as name_commerce',
        'sDireccion as address',
        'sRegion as region',
        'sCiudad as city',
        'sComuna as comuna',
        'nLat as latitude',
        'nLng as longitude',
        "sGiro as type",
        'sTelefono as telephone',
        'sTelefonoMovilContacto as mobile',
        'sContacto as conatct',
        'sEmailContacto as email',
        'sPaginaWeb as website',
        'sVendedor as vendor',
        'sEstado as state',
        'sTipoRecarga as type_recharge',
        'sTipoEmpresa as type_commerce',
        'sComentario as comment'
      ])
      .where('sEmpresa', id)
      .first()

      if(!boEmployer) throw new GE.HttpException('Tuvimos problemas configurando el usuario. Por favor comunicate a hola@amipass.com para solucionarlo a la brevedad\r(COD USRA404)', 404)

      const employer = await Employer.findOrCreate(
        { rut: boEmployer.rut },
        {
          name: boEmployer.name,
          rut: boEmployer.rut,
          name_commerce: boEmployer.name_commerce,
          address: boEmployer.address,
          region: boEmployer.region,
          city: boEmployer.city,
          comuna: boEmployer.comuna,
          latitude: boEmployer.latitude,
          longitude: boEmployer.longitude,
          type: (boEmployer.type) ? boEmployer.type : 'SIN ESPECIFICAR',
          telephone: boEmployer.telephone,
          mobile: boEmployer.mobile,
          conatct: boEmployer.conatct,
          email: boEmployer.email,
          website: boEmployer.website,
          vendor: boEmployer.vendor,
          state: boEmployer.state,
          type_recharge: boEmployer.type_recharge,
          type_commerce: boEmployer.type_commerce,
          comment: boEmployer.comment
        }
      )
      return {
        message: null,
        data: {
          employer,
          boEmployer
        }
      }
    } catch (error) {
      console.log('show Employer')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to update an existing employer.
   * GET employers/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit ({ params, request, response }) {
  }

  /**
   * Update employer details.
   * PUT or PATCH employers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a employer with id.
   * DELETE employers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = EmployerController
