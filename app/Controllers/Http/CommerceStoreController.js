'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with commercestores
 */

const GE = require('@adonisjs/generic-exceptions')
const Commerce = use('App/Models/Commerce');
const CommerceStore = use('App/Models/CommerceStore')
const BOTbLocales = use('App/Models/BO/TbLocale')
const Database = use('Database')
const Drive = use('Drive')
const getStream = require('into-stream');
const Env = use('Env');
const { AzureStorageBlob } = require('../../../Utils/AzureStorageBlob')

class CommerceStoreController {
  /**
   * Show a list of all commercestores.
   * GET commercestores
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response }) {
    const commerceStore = await CommerceStore.all()
    return {
      message: null,
      data: commerceStore
    }
  }

  /**
   * Render a form to be used for creating a new commercestore.
   * GET commercestores/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({ request, response }) {
  }

  /**
   * Create/save a new commercestore.
   * POST commercestores
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single commercestore.
   * GET commercestores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {
    try {
      const { commerce, store } = params

      const stores = await BOTbLocales.query().select([
        'sEstablecimiento as commerce',
        'sLocal as store',
        'sDescripcion as name',
        'sDireccion as address',
        'sDireccionComplemento as address2',
        'sComuna as comuna',
        'sCiudad as city',
        'sRegion as region',
        'sCalleDireccion as street',
        'sNumeroDireccion as number',
        'nLng as longitude',
        'nLat as latitude',
        'sEstado as state',
        'sClasificacion as tag',
        'sTelefonoLocal as phone',
        'sContactoMovil as mobile',
        'sComentario as comment'
      ])
        .where({
          'sEstablecimiento': commerce, 'sLocal': store, 'sEstado': 'AC'
        }).first()
      return {
        message: null,
        data: stores
      }
    } catch (error) {
      console.log('show Commerce Store')
      console.log(error)
      throw error
    }
  }

  /**
   * Display a single commercestore.
   * GET commercestores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async showuserpay({ params, request, response }) {
    try {
      const { code } = params

      const stores = await BOTbLocales.query().select([
        'TbLocales.sEstablecimiento as commerce',
        'TbLocales.sLocal as store',
        'TbUsuariosPay.sUsuario as userPay',
        'sDescripcion as name',
        'sDireccion as address',
        'sDireccionComplemento as address2',
        'sComuna as comuna',
        'sCiudad as city',
        'sRegion as region',
        'sCalleDireccion as street',
        'sNumeroDireccion as number',
        'nLng as longitude',
        'nLat as latitude',
        'sEstado as state',
        'sClasificacion as tag',
        'sTelefonoLocal as phone',
        'sContactoMovil as mobile',
        'sComentario as comment'
      ])
        .leftJoin('TbUsuariosPay', function () {
          this
            .on('TbUsuariosPay.sEstablecimiento', 'TbLocales.sEstablecimiento')
            .andOn('TbUsuariosPay.sLocal', 'TbLocales.sLocal')
        })
        .where({
          'sUsuario': code, 'sEstado': 'AC'
        }).first()

      if (!stores) throw new GE.HttpException('Por favor ingresa un código de comercio válido. \r(COD LUPA404)', 404)

      return {
        message: null,
        data: stores
      }
    } catch (error) {
      console.log('show Commerce Store')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to update an existing commercestore.
   * GET commercestores/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({ params, request, response }) {
  }

  /**
   * Update commercestore details.
   * PUT or PATCH commercestores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  async updateExtends({ params, request, response }) {
    try {
      const { commerce, store, delivery, withdrawal, urlcommerce, buttonpay } = request.body;
      //Busqueda de comercios
      const boCommerce = await Database.connection('mssql').raw(`
          select
                sRazonSocial [name],
                sRUT [rut],
                sDireccion [address],
                sTelefono [phone],
                sEmailContacto [email],
                sPaginaWeb [website],
                null [logo],
                sDescripcion [name_commerce],
                sRegion [region],
                sCiudad [city],
                sComuna [comuna],
                sEstado [state]
        from TbEstablecimientos where sEstablecimiento = :commerce
      `, {
        commerce
      })
      //Creación de comercios en PG
      const commercePG = await Commerce.findOrCreate({
        rut: boCommerce[0].rut
      }, {
        name: boCommerce[0].name,
        rut: boCommerce[0].rut,
        address: boCommerce[0].address,
        phone: boCommerce[0].phone,
        email: boCommerce[0].email,
        website: boCommerce[0].website,
        logo: null,
        name_commerce: boCommerce[0].description,
        region: boCommerce[0].region,
        city: boCommerce[0].city,
        comuna: boCommerce[0].comuna,
        state: boCommerce[0].state
      })

      const boStore = await Database.connection('mssql').raw(`
        select
              sDescripcion [name],
              sDireccion [address],
              nLat [latitude],
              nLng [longitude],
              sRegion [region],
              sCiudad [city],
              sComuna [comuna],
              sEstado [state],
              sComentario [comment],
              sLocal [store]
        from TbLocales where sEstablecimiento = :commerce and sLocal = :store
      `, {
        commerce,
        store
      })
      const commerceStore = await CommerceStore.findOrCreate({
        idcommerce: commercePG.idcommerce,
        bo_code: boStore[0].store
      }, {
        idcommerce: commercePG.idcommerce,
        name: boStore[0].name,
        rut: commercePG.rut,
        address: boStore[0].address,
        latitude: boStore[0].latitude,
        longitude: boStore[0].longitude,
        region: boStore[0].region,
        city: boStore[0].region,
        comuna: boStore[0].region,
        state: boStore[0].state,
        comment: boStore[0].comment,
        bo_code: boStore[0].store,
        delivery,
        withdrawal,
        urlcommerce,
        buttonpay
      })

      commerceStore.delivery = delivery
      commerceStore.withdrawal = withdrawal
      commerceStore.urlcommerce = urlcommerce
      commerceStore.buttonpay = buttonpay
      await commerceStore.save()

      return commerceStore
    } catch (error) {
      console.log('update extends Commerce Store')
      console.log(error)
      throw error
    }
  }

  /**
   * Delete a commercestore with id.
   * DELETE commercestores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  async uploadImageAzure({ params, request, response }) {
    try {

      let { commerce, store } = params

      const result = await Database.connection('mssql').raw(`
        select
          l.sEstablecimiento as commerce,
          l.sLocal as store,
          lf.iFotoChica as logo
        from TbLocales l
        left join TbLocalesFotos lf on l.sEstablecimiento = lf.sEstablecimiento and l.sLocal = lf.sLocal
        where l.sEstablecimiento = :commerce and l.sLocal = :store
        `, {
        commerce,
        store
      })
      if (result[0].logo !== null) {
        await Drive.put(`commerces/${commerce}/${store}/logo.png`, result[0].logo)

        const containerClient = new AzureStorageBlob().AzureStorageClient()

        const blobName = `${commerce}/${store}/logo.png`;
        const stream = getStream(await Drive.get(`commerces/${commerce}/${store}/logo.png`));
        const blockBlobClient = containerClient.getBlockBlobClient(blobName);

        const ONE_MEGABYTE = 1024 * 1024;

        await blockBlobClient.uploadStream(
          stream,
          4 * ONE_MEGABYTE,
          20,
          { blobHTTPHeaders: { blobContentType: "image/*" } }
        );

        return { saved: true }
      } else {
        return { saved: false }
      }
    } catch (error) {
      console.log('upload image azure Commerce Store')
      console.log(error)
      throw error
    }
  }

  async showImageAzure({ params, request, response }) {
    try {
      let { commerce, store } = params
      return `https://assetsamipass.blob.core.windows.net/commerces/${commerce}/${store}/logo.png`

    } catch (error) {
      console.log('show image azure Commerce Store')
      console.log(error)
      throw error
    }
  }

  async listImageAzure({ params, request, response }) {
    try {

      const containerClient = new AzureStorageBlob().AzureStorageClient()

      let paths = []

      let i = 1;
      let blobs = containerClient.listBlobsFlat();
      for await (const blob of blobs) {
        paths.push(`https://assetsamipass.blob.core.windows.net/commerces/${blob.name}`)
      }

      return paths
    } catch (error) {
      console.log('list image azure Commerce Store')
      console.log(error)
      throw error
    }
  }

  async deleteImageAzure({ params, request, response }) {
    try {

      let { commerce, store } = params

      const containerClient = new AzureStorageBlob().AzureStorageClient()
      const blobName = `${commerce}/${store}/logo.png`;
      containerClient.deleteBlob(blobName)

    } catch (error) {
      console.log('delete image azure Commerce Store')
      console.log(error)
      throw error
    }
  }

  async uploadCover({ params, request, response }) {
    try {

      let { code } = params

      const containerClient = new AzureStorageBlob().AzureStorageClient()

      const blobName = `covers/${code}.jpg`;
      const stream = getStream(await Drive.get(`covers/${code}.jpg`));
      const blockBlobClient = containerClient.getBlockBlobClient(blobName);

      const ONE_MEGABYTE = 1024 * 1024;

      await blockBlobClient.uploadStream(
        stream,
        4 * ONE_MEGABYTE,
        20,
        { blobHTTPHeaders: { blobContentType: "image/*" } }
      );

      return { saved: true }
    } catch (error) {
      console.log('upload cover azure Commerce Store')
      console.log(error)
      throw error
    }
  }

  async migrateImagesAzure({ params, request, response }) {
    try {

      const commerces = await Database.connection('mssql').raw(`
        select
          l.sEstablecimiento as commerce,
          l.sLocal as store,
          lf.iFotoChica as logo
        from TbLocales l
        left join TbLocalesFotos lf on l.sEstablecimiento = lf.sEstablecimiento and l.sLocal = lf.sLocal
        where (l.sEstado = 'AC')
        and l.bActivarMapa=1
        `)

      const containerClient = new AzureStorageBlob().AzureStorageClient()
      const ONE_MEGABYTE = 1024 * 1024;
      commerces.map(async (item) => {
        let { commerce, store, logo } = item
        await Drive.put(`commerces/${commerce}/${store}/logo.png`, logo)
      })
      // commerces.map(async (item) => {
      //   let { commerce, store, logo } = item
        
      //   let blobName = `${commerce}/${store}/logo.png`;
      //   let stream = getStream(await Drive.get(`commerces/${commerce}/${store}/logo.png`));
      //   let blockBlobClient = containerClient.getBlockBlobClient(blobName);

      //   await blockBlobClient.uploadStream(
      //     stream,
      //     4 * ONE_MEGABYTE,
      //     20,
      //     { blobHTTPHeaders: { blobContentType: "image/*" } }
      //   ).then(response => {
      //     console.log(commerce, store, 'image migrated')
      //   });
      // })
      return {
        message: null,
        data: commerces.length
      }
    } catch (error) {
      console.log('migrate image azure Commerce Store')
      console.log(error)
      throw error
    }
  }
}

module.exports = CommerceStoreController
