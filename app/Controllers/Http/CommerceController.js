'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with commerce
 */

const Drive = use('Drive')
const Database = use('Database')
const axios = require('axios');
const Commerce = use('App/Models/Commerce');
const CommerceStore = use('App/Models/CommerceStore')
const CommercesTag = use('App/Models/CommerceTag')
const TagsBo = use('App/Models/TagsBo')
const Tags = use('App/Models/Tag')
const BOCommerce = use('App/Models/BO/TbEstablecimiento')
const BOCommerceStore = use('App/Models/BO/TbLocale')
const BOCommune = use('App/Models/BO/TbComunas')
const { Geocoding } = require('../../../Utils/GoogleGeocodingAPI')
// const CommerceStoreController = require('./CommerceStoreController');
const Env = use('Env');

class CommerceController {

  /**
   * Show a list of all commerce.
   * GET commerce
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response, params }) {
    try {
      const { latitude, longitude, filters } = request.body
      const { size, page, tag, tags, recent, delivery, withdrawal } = (typeof filters !== 'undefined') ? filters :
        {
          size: 10,
          page: 1,
          tag: null,
          tags: null,
          recent: false,
          delivery: false,
          withdrawal: false
        }

      let kTags = null

      if (tag !== null) {
        kTags = ''
        let tagsBo = await TagsBo.query()
          .where({
            idtags: tag
          }).pair('bo_code', 'name')

        Object.keys(tagsBo).map(item => {
          kTags += `'${item}'`
        })
        kTags = kTags.replace(/''/g, "','")
      }

      if (tags) {
        if (tags.length > 0) {
          kTags = ''
          let vTag = []
          tags.map(item => {
            vTag.push(item.tag)
          })
          let tagsBo = await Database
            .select('bo_code')
            .from('tags_bo')
            .whereIn('idtags', vTag)

          Object.values(tagsBo).map(item => {
            kTags += `'${item.bo_code}'`
          })
          kTags = kTags.replace(/''/g, "','")
        }
      }

      let commune = null
      let communeCode = null
      if(delivery) {
        let { boCommune, boCommuneCode } = await new Geocoding().ReverseGeocodingCommune(latitude, longitude)
          .then(async response => {
            let boCommune = await BOCommune.query()
              .where('sDescripcion', 'like', `${response.commune}`)
              .first()
            if (boCommune) {
              let { sComuna, sDescripcion } = boCommune
              return { boCommuneCode: sComuna, boCommune: sDescripcion }
            } else {
              return { boCommuneCode: null, boCommune: null }
            }
          })
        communeCode = boCommuneCode
        commune = boCommune
      }


      console.log(commune, communeCode)

      const commerces = await Database.connection('mssql').raw(
        `
			;WITH PageNumbers AS (
			SELECT ROW_NUMBER() OVER(ORDER BY dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude)) row,
				l.sEstablecimiento + l.sLocal code,
				l.sEstablecimiento commerce,
				upay.sUsuario userPay,
				l.sLocal store,
				e.sRut rut,
				e.sRazonSocial name,
				l.sDescripcion alias,
				l.sDireccion + ' ' + ISNULL(l.sDireccionComplemento, '') address,
				co.sComuna cocomuna,
				co.sDescripcion comuna,
				re.sRegion coregion,
        re.sDescripcion region,
        l.sContactoTelefono telContact,
        l.sTelefonoLocal telStore,
        l.sContactoMovil telMobile,
				l.nLat latitude,
				l.nLng longitude,
        dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude) distance,
        l.bLogo logoActive,
				CASE WHEN 
					cast(l.dActivacion as date) between cast((getdate() -90) as date) and cast(getdate() as date) THEN 1
				ELSE 0 END as newLocal,
				l.bAppPAY payApp,
				l.bSmartphoneIntegrado payQR,
				l.bSmartphone payCC,
				l.bH2H paykDyn,
				l.bPosGrps payCard,
        l.bPosEthernet payCard2,
        l.bRepartoDelivery delivery,
        l.bRepartoRetiro withdrawal
			FROM TbLocales AS l
				INNER JOIN TbEstablecimientos AS e ON l.sEstablecimiento = e.sEstablecimiento
				left join TbRegiones re on l.sRegion = re.sRegion
				left join TbComunas co on co.sComuna = l.sComuna
				left join TbUsuariosPay upay on l.sEstablecimiento = upay.sEstablecimiento and l.sLocal = upay.sLocal
        left join TbLocalesClasificacion cla on l.sLocal = cla.sLocal and l.sEstablecimiento = cla.sEstablecimiento
			WHERE (l.sEstado = 'AC')
				${(typeof delivery === 'undefined') || !delivery ? 'and (dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude) between 0 and 20)' : ''}
				and bActivarMapa=1
        and ( ((:recent is null) or (:recent = 0)) or ((:recent is not null) and (cast(l.dActivacion as date) between cast((getdate() -90) as date) and cast(getdate() as date) )) )
        and ( ((:withdrawal is null) or (:withdrawal = 0)) or ((:withdrawal is not null) and (l.bRepartoRetiro = 1)) )
        and ( ((:tag is null) or (:tag = '')) or ((:tag is not null) and (cla.sClasificacion in (${kTags}))) )
        and ( ((:delivery is null) or (:delivery = 0)) or ((:delivery is not null) and (l.bRepartoDelivery = 1)) )
        and ( ((:delivery is null) or (:delivery = 0)) or ((:delivery is not null) and (l.sEstablecimiento+l.sLocal in (select lc.sEstablecimiento+lc.sLocal from TbLocalesCobertura lc where lc.sComuna = '${communeCode}'))) )
        group by
        l.sEstablecimiento + l.sLocal ,
        l.sEstablecimiento ,
        upay.sUsuario ,
        l.sLocal ,
        e.sRut ,
        e.sRazonSocial ,
        l.sDescripcion ,
        l.sDireccion + ' ' + ISNULL(l.sDireccionComplemento, ''),
        co.sComuna ,
        co.sDescripcion ,
        re.sRegion ,
        re.sDescripcion ,
        l.sContactoTelefono ,
        l.sTelefonoLocal ,
        l.sContactoMovil ,
        l.nLat ,
        l.nLng,
        l.bLogo,
        l.bAppPAY,
        l.bSmartphoneIntegrado,
        l.bSmartphone,
        l.bH2H,
        l.bPosGrps,
        l.bPosEthernet,
        l.bRepartoDelivery,
        l.bRepartoRetiro,
        l.dActivacion
			)
			SELECT  *
			FROM    PageNumbers
			WHERE   row  BETWEEN ((:page - 1) * :size + 1)
					AND (:page * :size)
			`,
        {
          latitude: `${latitude}`,
          longitude: `${longitude}`,
          size,
          page: (page == 0) ? 1 : page,
          recent: (typeof recent === 'undefined') ? null : recent,
          delivery: (typeof delivery === 'undefined') ? null : delivery,
          withdrawal: (typeof withdrawal === 'undefined') ? null : withdrawal,
          tag: (typeof tag === 'undefined') ? null : kTags,
        })

      let result = commerces.map(async (item) => {
        const boLogo = await Database.connection('mssql').raw(`
        select
          lf.iFotoChica as logo
        from TbLocales l
        left join TbLocalesFotos lf on l.sEstablecimiento = lf.sEstablecimiento and l.sLocal = lf.sLocal
        where l.sEstablecimiento = :commerce and l.sLocal = :store
        `, {
          commerce: item.commerce,
          store: item.store
        })

        // const commerceStore = new CommerceStoreController()
        // commerceStore.uploadImageAzure({ params: { commerce: item.commerce, store: item.store } })

        // item['logo'] = !(boLogo[0].logo === null)

        item['logo'] = boLogo[0].logo

        try {
          if (item['logo'] !== null) {
            await Drive.put(`commerces/${item.commerce}/${item.store}/logo.png`, item['logo'])
          }
        } catch (error) {
          console.log('Creando logo del comercio')
          console.log(error)
        }

        const boPromociones = await Database.connection('mssql').raw(`
					select count(idPromocion) [count] from TbPromocionesNuevas
					where sEstablecimiento = :commerce
					and (sLocal = :store or sLocal = 'Todos')
					and bAprobacionAmipass = :authorized
          and cast(getdate() as date) between cast(dInicio as date) and cast(dTermino as date)
          and sTipo <> 'DescuentoSKU'
				`, {
          commerce: item.commerce,
          store: item.store,
          authorized: true
        })

        item['withPromotions'] = (boPromociones[0].count > 0)

        const boPromocionesSku = await Database.connection('mssql').raw(`
					select count(idPromocion) [count] from TbPromocionesNuevas
					where sEstablecimiento = :commerce
					and (sLocal = :store or sLocal = 'Todos')
					and bAprobacionAmipass = :authorized
          and cast(getdate() as date) between cast(dInicio as date) and cast(dTermino as date)
          and sTipo = 'DescuentoSKU'
				`, {
          commerce: item.commerce,
          store: item.store,
          authorized: true
        })

        item['amibox'] = (boPromocionesSku[0].count > 0)

        try {
          //Busqueda de comercios
          const boCommerce = await Database.connection('mssql').raw(`
						select
							sRazonSocial [name],
							sRUT [rut],
							sDireccion [address],
							sTelefono [phone],
							sEmailContacto [email],
							sPaginaWeb [website],
							null [logo],
							sDescripcion [name_commerce],
							sRegion [region],
							sCiudad [city],
							sComuna [comuna],
							sEstado [state]
					from TbEstablecimientos where sEstablecimiento = :commerce
					`, {
            commerce: item.commerce
          })

          let urlCommerce = null
          let commerceStore = null
          //Creación de comercios en PG
          try {
            let commerce = await Commerce.query()
              .where({
                'rut': boCommerce[0].rut
              }).first()

            if (commerce === null) {
              commerce = await Commerce.create({
                name: boCommerce[0].name,
                rut: boCommerce[0].rut,
                address: boCommerce[0].address,
                phone: boCommerce[0].phone,
                email: boCommerce[0].email,
                website: boCommerce[0].website,
                logo: null,
                name_commerce: boCommerce[0].description,
                region: boCommerce[0].region,
                city: boCommerce[0].city,
                comuna: boCommerce[0].comuna,
                state: boCommerce[0].state,
                bo_code: item.commerce
              })
            } else {
              commerceStore = await CommerceStore
                .query()
                .where({
                  'idcommerce': commerce.idcommerce,
                  'bo_code': item.store
                }).first()
              if (commerceStore === null) {
                try {
                  commerceStore = await CommerceStore.create({
                    idcommerce: commerce.idcommerce,
                    name: item.alias,
                    rut: item.rut,
                    address: item.address,
                    latitude: item.latitude,
                    longitude: item.longitude,
                    region: item.coregion,
                    city: null,
                    comuna: item.cocomuna,
                    state: item.state,
                    comment: item.comment,
                    bo_code: item.store
                  })
                } catch (error) {
                  console.log('Creando local')
                  console.log(error)
                }
              }
              urlCommerce = commerceStore.urlcommerce
            }
          } catch (error) {
            console.log('Creando comercio')
            console.log(error)
          }

          item['urlcommerce'] = urlCommerce

          const boTags = await Database.connection('mssql').raw(`
					select 
							sEstablecimiento [commerce], 
							sLocal [store], 
              sClasificacion [tag],
              dom.sDescripcion [name]
					from TbLocalesClasificacion cla
          inner join TbDominios dom on dom.sCodigo = cla.sClasificacion and dom.sDominio = 'CLASIFICACION_LOCAL'
					where sEstablecimiento = :commerce and sLocal = :store
					`, {
            commerce: item.commerce,
            store: item.store
          })
          if (boTags.length > 0) {
            let tags = await Promise.all(await boTags.map(async itemTag => {
              try {
                const tag = await TagsBo.query()
                  .where({
                    bo_code: itemTag.tag
                  }).first()
                await CommercesTag.findOrCreate({
                  idcommerce_store: commerceStore.idcommerce_store,
                  idtags: tag.idtags
                }, {
                  idcommerce_store: commerceStore.idcommerce_store,
                  idtags: tag.idtags
                })

                return {
                  name: itemTag.name,
                  tag: itemTag.tag
                }
              } catch (error) {
                console.log('Asignando clasificación')
                console.log(error)
                return {
                  name: "Sin Categoría",
                  tag: null
                }
              }
            }))
            try {
              item['tags'] = tags
              item['tagCode'] = tags[0].tag
              item['tag'] = tags[0].name
            } catch (error) {
              console.log('Clasificaciones')
              console.log(boTags)
            }
          } else {
            item['tags'] = [{ tag: null, name: 'Sin Categoría' }]
            item['tagCode'] = null
            item['tag'] = 'Sin Categoría'
          }

        } catch (error) {
          console.log('Busqueda de comercio en BO')
          console.log(error)
        }

        return await item
      })

      const data = await Promise.all(result.map((element, index) => {
        return element
      }))
        .then(result => {
          return result
        })
      return {
        message: null,
        data
      }
    } catch (error) {
      console.log('Index Commerce')
      console.log(error)
      throw error
    }
  }

  async withAmibox({ request, response, params }) {
    try {
      const { latitude, longitude, filters } = request.body
      const { size, page } = (typeof filters !== 'undefined') ? filters :
        {
          size: 10,
          page: 1
        }

      const commerces = await Database.connection('mssql').raw(
        `
			;WITH PageNumbers AS (
        SELECT ROW_NUMBER() OVER(ORDER BY dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude)) row,
          l.sEstablecimiento + l.sLocal code,
          l.sEstablecimiento commerce,
          upay.sUsuario userPay,
          l.sLocal store,
          e.sRut rut,
          e.sRazonSocial name,
          l.sDescripcion alias,
          l.sDireccion + ' ' + ISNULL(l.sDireccionComplemento, '') address,
          co.sComuna cocomuna,
          co.sDescripcion comuna,
          re.sRegion coregion,
          re.sDescripcion region,
          l.nLat latitude,
          l.nLng longitude,
          dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude) distance,
          l.bLogo logoActive,
          lf.iFotoChica as logo,
          l.bAppPAY payApp,
          l.bSmartphoneIntegrado as payQR,
          l.bSmartphone as payCC,
          l.bH2H paykDyn,
          l.bPosGrps payCard,
          l.bPosEthernet payCard2,
          l.bRepartoDelivery as delivery,
          l.bRepartoRetiro as withdrawal
        FROM TbLocales AS l
          INNER JOIN TbEstablecimientos AS e ON l.sEstablecimiento = e.sEstablecimiento
          left join TbRegiones re on l.sRegion = re.sRegion
          left join TbComunas co on co.sComuna = l.sComuna
          left join TbUsuariosPay upay on l.sEstablecimiento = upay.sEstablecimiento and l.sLocal = upay.sLocal
          left join TbLocalesFotos lf on l.sEstablecimiento = lf.sEstablecimiento and l.sLocal = lf.sLocal
        WHERE (l.sEstado = 'AC')
          AND (dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude) between 0 and 20)
          and bActivarMapa=1
          and (select count(idPromocion) from TbPromocionesNuevas where sTipo = 'DescuentoSKU' and sEstablecimiento = l.sEstablecimiento and ((sLocal = l.sLocal) or (sLocal = 'Todos')) and bAprobacionAmipass = :authorized
          and cast(getdate() as date) between cast(dInicio as date) and cast(dTermino as date) ) > 0
			)
			SELECT  *
			FROM    PageNumbers
			WHERE   row  BETWEEN ((:page - 1) * :size + 1)
					AND (:page * :size)
			`,
        {
          latitude: `${latitude}`,
          longitude: `${longitude}`,
          size,
          page,
          authorized: true
        })

      let result = commerces.map(async (item) => {
        try {
          if (item.logo !== null) {
            await Drive.put(`commerces/${item.commerce}/${item.store}/logo.png`, item.logo)
          }
        } catch (error) {
          console.log('Creando la imágen del amibox')
          console.log(error)
        }

        const boPromociones = await Database.connection('mssql').raw(`
					select count(idPromocion) [count] from TbPromocionesNuevas
					where sEstablecimiento = :commerce
					and (sLocal = :store or sLocal = 'Todos')
					and bAprobacionAmipass = :authorized
          and cast(getdate() as date) between cast(dInicio as date) and cast(dTermino as date)
          and sTipo <> 'DescuentoSKU'
				`, {
          commerce: item.commerce,
          store: item.store,
          authorized: true
        })

        item['withPromotions'] = (boPromociones[0].count > 0)

        //Creación de comercios en PG
        try {
          let commerce = await Commerce.query()
            .where({
              'rut': boCommerce[0].rut
            }).first()

          if (commerce === null) {
            commerce = await Commerce.create({
              name: boCommerce[0].name,
              rut: boCommerce[0].rut,
              address: boCommerce[0].address,
              phone: boCommerce[0].phone,
              email: boCommerce[0].email,
              website: boCommerce[0].website,
              logo: null,
              name_commerce: boCommerce[0].description,
              region: boCommerce[0].region,
              city: boCommerce[0].city,
              comuna: boCommerce[0].comuna,
              state: boCommerce[0].state,
              bo_code: item.commerce
            })
          } else {
            let commerceStore = await CommerceStore
              .query()
              .where({
                'idcommerce': commerce.idcommerce,
                'bo_code': item.store
              }).first()
            if (commerceStore === null) {
              try {
                commerceStore = await CommerceStore.create({
                  idcommerce: commerce.idcommerce,
                  name: item.alias,
                  rut: item.rut,
                  address: item.address,
                  latitude: item.latitude,
                  longitude: item.longitude,
                  region: item.coregion,
                  city: null,
                  comuna: item.cocomuna,
                  state: item.state,
                  comment: item.comment,
                  bo_code: item.store
                })
              } catch (error) {
                console.log('Creando local')
                console.log(error)
              }
            }
            urlCommerce = commerceStore.urlcommerce
          }
        } catch (error) {
          console.log('Creando comercio')
          console.log(error)
        }

        item['urlcommerce'] = urlCommerce

        try {
          const boTags = await Database.connection('mssql').raw(`
					select 
							sEstablecimiento [commerce], 
							sLocal [store], 
              sClasificacion [tag],
              dom.sDescripcion [name]
					from TbLocalesClasificacion cla
          inner join TbDominios dom on dom.sCodigo = cla.sClasificacion and dom.sDominio = 'CLASIFICACION_LOCAL'
					where sEstablecimiento = :commerce and sLocal = :store
					`, {
            commerce: item.commerce,
            store: item.store
          })
          item['amibox'] = true
          if (boTags.length > 0) {
            let tags = await Promise.all(await boTags.map(async itemTag => {
              return {
                name: itemTag.name,
                tag: itemTag.tag
              }
            }))
            try {
              item['tags'] = tags
              item['tagCode'] = tags[0].tag
              item['tag'] = tags[0].name
            } catch (error) {
              console.log('Clasificación en amibox')
              console.log(boTags)
            }
          } else {
            item['tags'] = [{ tag: null, name: 'Sin Categoría' }]
            item['tagCode'] = null
            item['tag'] = 'Sin Categoría'
          }
        } catch (error) {
          console.log('Clasificaciones en amibox')
          console.log(error)
        }
        return await item
      })
      const data = await Promise.all(result.map((element, index) => {
        return element
      })).then(result => {
        return result
      })
      return {
        message: null,
        data
      }
    } catch (error) {
      console.log('withAmibox Commerce')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new commerce.
   * GET commerce/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({
    request,
    response
  }) { }

  /**
   * Create/save a new commerce.
   * POST commerce
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({
    request,
    response
  }) { }

  /**
   * Display a single commerce.
   * GET commerce/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {
    try {
      const { id } = params
      //consulta comercio en BO
      const boCommerce = await BOCommerce.query()
        .select([
          'sEstablecimiento as id',
          'sRazonSocial as name',
          'sRUT as rut',
          'sDireccion as address',
          'sTelefono as phone',
          'sEmailContacto as email',
          'sPaginaWeb as website',
          'sDescripcion as description',
          'sGiro as type',
          'sComuna as comuna',
          'sCiudad as city',
          'sRegion as region',
          'sContacto as contact',
          'sTelefonoMovilContacto as phoneMobile',
          'sEstado as state',
        ])
        .where('sEstablecimiento', id)
        .first()

      //busca en comercios y crea si no existe
      const commerce = await Commerce.findOrCreate({
        rut: boCommerce.rut
      }, {
        name: boCommerce.name,
        rut: boCommerce.rut,
        address: boCommerce.address,
        phone: boCommerce.phone,
        email: boCommerce.email,
        website: boCommerce.website,
        rank: 0,
        logo: null,
        name_commerce: boCommerce.description,
        region: boCommerce.region,
        city: boCommerce.city,
        comuna: boCommerce.comuna,
        state: boCommerce.state
      })

      //busca tiendas/locales del comercio
      const boCommerceStores = await BOCommerceStore.query()
        .select([
          'sEstablecimiento as commerce',
          'sLocal as store',
          'sDescripcion as name',
          'sDireccion as address',
          'sDireccionComplemento as address2',
          'sRegion as region',
          'sCiudad as city',
          'sComuna as comuna',
          'sCalleDireccion as street',
          'sNumeroDireccion as number',
          'bFacturacionIndependiente as invoice',
          'nLng as latitude',
          'nLat as longitude',
          'sEstado as state',
          'sComentario as comment'
        ])
        .where('sEstablecimiento', id)
        .fetch()

      // consulta cada una de las tiendas 
      // para registrarlas en caso de que no exista
      const commerceStores = boCommerceStores.rows.map(async (value) => {
        return await CommerceStore.findOrCreate({
          idcommerce: commerce.idcommerce,
          bo_code: value.store
        }, {
          idcommerce: commerce.idcommerce,
          name: value.name,
          rut: commerce.rut,
          address: value.address,
          latitude: value.latitude,
          longitude: value.longitude,
          region: value.region,
          city: value.city,
          comuna: value.comuna,
          invoice: value.invoice,
          state: value.state,
          comment: value.comment,
          bo_code: value.store
        })
      })

      return {
        message: null,
        data: {
          commerce,
          commerceStores,
          boCommerce,
          boCommerceStores,
        }
      }
    } catch (error) {
      console.log('Clasificación en amibox')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to update an existing commerce.
   * GET commerce/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({
    params,
    request,
    response
  }) { }

  /**
   * Update commerce details.
   * PUT or PATCH commerce/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({
    params,
    request,
    response
  }) { }

  /**
   * Delete a commerce with id.
   * DELETE commerce/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({
    params,
    request,
    response
  }) { }

  async getTags() {
    try {
      const tags = await Tags.all();
      return {
        message: "ok",
        data: {
          tags
        }
      }
    } catch (error) {
      console.log('getTags Commerce')
      console.log(error)
      throw error
    }
  }


  async commerceCoverage({ params, request, response }) {
    const { sEstablecimiento, sLocal } = request.body;

    const result = await axios({
      url: `${Env.get('API_V1')}/CoberturaLocal`,
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      data: JSON.stringify({
        sEstablecimiento: sEstablecimiento,
        sLocal: sLocal
      })
    }).then((data) => {
      return data.data
    }).catch(error => {
      console.log('Cobertura del local')
      console.log(error)
      throw new GE.HttpException('Error al intentar traer cobertura de locales.\r(COD CCOB404)', 404)
    })
    return {
      message: "ok",
      data: {
        result
      }
    }
  }
}

module.exports = CommerceController
