'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const UserTransferFavorite = use('App/Models/UserTransferFavorite')
const User = use('App/Models/User')
const Database = use('Database')
/**
 * Resourceful controller for interacting with transferfavoriteaccounts
 */
class TransferFavoriteAccountController {
  /**
   * Show a list of all transferfavoriteaccounts.
   * GET transferfavoriteaccounts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response, auth }) {
    try {
      const { username, employer } = request.body
      
      const user = await User.query().where({
        username
      }).first()
      
      const favorite = await UserTransferFavorite.query()
        .where({
          idusers: user.idusers,
          employer
        }).fetch()
        
      const fav = favorite.rows.map(item => {
        return item.account
      })
      
      const result = await Database.connection('mssql')
        .select([
          'sEmpresa as employer',
          'sEmpleado as code',
          'sNombre as name',
          'sApellidoPaterno as lastname',
          'sAPellidoMaterno as lastname2',
          'sRUT as rut'
        ])
        .from('TbEmpleados')
        .where('sEmpresa', employer)
        .whereIn('sEmpleado', fav)

      return {
        data: result
      }
    } catch (error) {
      console.log('index Transfer Favorite')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new transferfavoriteaccount.
   * GET transferfavoriteaccounts/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create ({ request, response, auth }) {
    try {
      const { account, employer } = request.body
      const { idusers } = await auth.getUser()
      const favorite = await UserTransferFavorite({
        idusers,
        account,
        employer
      })
      return {
        message: {
          title: null,
          text: null
        },
        data: favorite
      }
    } catch (error) {
      console.log('create Transfer Favorite')
      console.log(error)
    }
  }

  /**
   * Create/save a new transferfavoriteaccount.
   * POST transferfavoriteaccounts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single transferfavoriteaccount.
   * GET transferfavoriteaccounts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
  }

  /**
   * Render a form to update an existing transferfavoriteaccount.
   * GET transferfavoriteaccounts/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit ({ params, request, response }) {
  }

  /**
   * Update transferfavoriteaccount details.
   * PUT or PATCH transferfavoriteaccounts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a transferfavoriteaccount with id.
   * DELETE transferfavoriteaccounts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = TransferFavoriteAccountController
