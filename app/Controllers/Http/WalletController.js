'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Wallet = use('App/Models/Wallet');

/**
 * Resourceful controller for interacting with wallets
 */
class WalletController {
  /**
   * Show a list of all wallets.
   * GET wallets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    try {
      const { idusers } = auth.user

      const wallet = await Wallet.query().where([{
        'user_id': idusers
      }]).fetch()

      response.status().send({
        message: '',
        data: wallet
      })
    } catch (error) {
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new wallet.
   * GET wallets/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new wallet.
   * POST wallets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single wallet.
   * GET wallets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view, auth }) {
    try {
      const { id } = params
      const { idusers } = auth.user

      const wallet = await Wallet.query().where([{
        'id': id,
        'user_id': idusers
      }]).first()

      response.status().send({
        message: '',
        data: wallet
      })
    } catch (error) {
      throw error
    }
  }

  /**
   * Render a form to update an existing wallet.
   * GET wallets/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update wallet details.
   * PUT or PATCH wallets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response, auth }) {
    try {
      const { id } = params
      const { idusers } = auth.user
      const { name, description, active, business, codeReference } = request.body

      const wallet = await Wallet.query()
      .where([{
        'id': id,
        'user_id': idusers
      }])
      .update({ 
        name,
        description,
        active,
        business,
        codeReference
      })

      response.status().send({
        message: '',
        data: wallet
      })
    } catch (error) {
      throw error
    }
  }

  /**
   * Delete a wallet with id.
   * DELETE wallets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  /**
   * Update wallet details.
   * PUT or PATCH wallets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async changeState({ params, request, response, auth }) {
    try {
      const { idusers } = auth.user
      const { id, active } = request.body

      const wallet = await Wallet.query()
      .where([{
        'id': id,
        'user_id': idusers
      }])
      .update({
        active,
      })

      response.status().send({
        message: '',
        data: wallet
      })
    } catch (error) {
      throw error
    }
  }
}

module.exports = WalletController
