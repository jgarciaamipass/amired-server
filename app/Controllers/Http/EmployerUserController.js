'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with employerusers
 */

const GE = require('@adonisjs/generic-exceptions')
const EmployerUser = use('App/Models/EmployerUser')
const BOTbEmpleado = use('App/Models/BO/TbEmpleado')
const Database = use('Database');

class EmployerUserController {
  /**
   * Show a list of all employerusers.
   * GET employerusers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, response }) {
    const employerUser = await EmployerUser.all()
    return {
      message: null,
      data: employerUser
    }
  }

  /**
   * Render a form to be used for creating a new employeruser.
   * GET employerusers/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create ({ request, response }) {
  }

  /**
   * Create/save a new employeruser.
   * POST employerusers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single employeruser.
   * GET employerusers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
    try {
      const { employer, employee } = params
      const boTbEmpleado = await BOTbEmpleado.query().select([
        'sEmpleado as code',
        'sNombre as name',
        'sApellidoPaterno as lastname1',
        'sAPellidoMaterno as lastname2',
        'sRUT as rut'
      ])
      .where({
        'sEmpresa': employer,
        'sEmpleado': employee
      }).first()

      if(!boTbEmpleado) throw new GE.HttpException('Por favor ingrese un RUT o NN válido\r(COD EMPA404)', 404)
      
      return {
        message: null,
        data: boTbEmpleado
      }
    } catch (error) {
      console.log('show Employer User')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to update an existing employeruser.
   * GET employerusers/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit ({ params, request, response }) {
  }

  /**
   * Update employeruser details.
   * PUT or PATCH employerusers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a employeruser with id.
   * DELETE employerusers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = EmployerUserController
