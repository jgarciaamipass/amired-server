'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


/**
 * Resourceful controller for interacting with commerce
 */

const GE = require('@adonisjs/generic-exceptions')
const Env = use('Env')
const axios = require('axios')

class PaySmartQrController {
  /**
     * Show a list of all commerce.
     * GET commerce
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
  async index({ request, response, params }) {
    try {
      const { card } = request.body
      const result = await Database.connection('mssql').raw(
        `
        SELECT top 10
                    max(tra.dTransaccion) lastdate,
                    tra.sEstablecimiento commerce,
                    tra.sLocal store,
                    est.sDescripcion name,
                    est.sDireccion address
        from TbTransacciones tra
        left join TbEstablecimientos est on tra.sEstablecimiento = est.sEstablecimiento
        where tra.sTarjeta = :card
        group by tra.sEstablecimiento, tra.sLocal, est.sDescripcion, est.sDireccion
        order by 1 desc
        `,
        {
          card: `${card}`
        })

      return {
        message: null,
        data: result
      }
    } catch (error) {
      console.log('index Pay Smart QR')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new commerce.
   * GET commerce/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({
    request,
    response
  }) { }

  /**
   * Create/save a new commerce.
   * POST commerce
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const { employee, employer, pin, amount } = request.body
    try {
      const result = await axios({
        url: `${Env.get('API_V1')}/CodigoQR`,
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        data: JSON.stringify({
          sEmpleado: employee,
          sEmpresa: employer,
          sClave: pin,
          nMonto: amount
        })
      }).then((response) => {
        if (response.status === 200) {
          const { sQR } = response.data
          return {
            code: sQR
          }
        }
        throw new GE.HttpException('Error generando el QR o Saldo insuficiente\r(COD PQRB403)', 403)
      }).catch(error => {
        console.log('CodigoQR benlar en Pay Smart QR')
        console.log(error)
        if (error.response.status == 406) {
          throw new GE.HttpException('Por favor intente nuevamente.\r(COD PQRB403)', 403)
        }
        throw new GE.HttpException('Error generando el QR o Saldo insuficiente\r(COD PQRB500)', 500)
      })
      response.status(201).send(result)
    } catch (error) {
      console.log('store Pay Smart QR')
      console.log(error)
      throw error
    }
  }

  /**
   * Display a single commerce.
   * GET commerce/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {
    
  }

  /**
   * Render a form to update an existing commerce.
   * GET commerce/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({
    params,
    request,
    response
  }) { }

  /**
   * Update commerce details.
   * PUT or PATCH commerce/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({
    params,
    request,
    response
  }) { }

  /**
   * Delete a commerce with id.
   * DELETE commerce/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({
    params,
    request,
    response
  }) { }
}

module.exports = PaySmartQrController
