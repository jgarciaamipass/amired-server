'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const WalletType = use('App/Models/WalletType');

/**
 * Resourceful controller for interacting with wallettypes
 */
class WalletTypeController {
  /**
   * Show a list of all wallettypes.
   * GET wallettypes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    return {
      message: '',
      data: await WalletType.all()
    }
  }

  /**
   * Render a form to be used for creating a new wallettype.
   * GET wallettypes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {

  }

  /**
   * Create/save a new wallettype.
   * POST wallettypes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    try {
      const { name, description } = request.body()

      const walletType = await WalletType.create({
        name,
        description
      })

      response.status(201).send({
        message: '',
        data: walletType
      })
    } catch (error) {
      throw error
    }
  }

  /**
   * Display a single wallettype.
   * GET wallettypes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    try {
      const { id } = params

      const walletType = await WalletType.find(id)

      response.status().send({
        message: '',
        data: walletType
      })
    } catch (error) {
      throw error
    }
  }

  /**
   * Render a form to update an existing wallettype.
   * GET wallettypes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update wallettype details.
   * PUT or PATCH wallettypes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    try {
      const { id } = params
      const { name, description } = request.body

      const walletType = await WalletType.query()
        .where('id', id)
        .update({ name, description })

      response.status().send({
        message: '',
        data: walletType
      })
    } catch (error) {
      throw error
    }
  }

  /**
   * Delete a wallettype with id.
   * DELETE wallettypes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    try {
      const { id } = params

      const walletType = await WalletType.query()
        .where('id', id)
        .delete()

      response.status().send({
        message: '',
        data: walletType
      })
    } catch (error) {
      throw error
    }
  }
}

module.exports = WalletTypeController
