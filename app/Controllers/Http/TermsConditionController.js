'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const GE = require('@adonisjs/generic-exceptions')
const Terms = use('App/Models/Term');
const Database = use('Database');

/**
 * Resourceful controller for interacting with termsconditions
 */
class TermsConditionController {
  /**
   * Show a list of all termsconditions.
   * GET termsconditions
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    try {
      return {
        message: '',
        data: await Terms.all()
      }
    } catch (error) {
      console.log('index Terms and Conditions')
      console.log(error)
    }
  }

  /**
   * Render a form to be used for creating a new termscondition.
   * GET termsconditions/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new termscondition.
   * POST termsconditions
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    try {
      const { path, version, available } = request.body

      const terms = await Terms.create({
        path,
        version,
        available
      })

      response.status(201).send({
        message: '',
        data: terms
      })
    } catch (error) {
      console.log('stoew Terms and Conditions')
      console.log(error)
      throw error
    }
  }

  /**
   * Display a single termscondition.
   * GET termsconditions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    try {
      const { id } = params

      const terms = await Terms.find(id)

      response.status().send({
        message: '',
        data: terms
      })
    } catch (error) {
      console.log('show Terms and Conditions')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to update an existing termscondition.
   * GET termsconditions/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update termscondition details.
   * PUT or PATCH termsconditions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    try {
      const { id } = params
      const { path, version, available } = request.body

      const terms = await Terms.query()
        .where('id', id)
        .update({ path, version, available })

      response.status().send({
        message: '',
        data: terms
      })
    } catch (error) {
      console.log('update Terms and Conditions')
      console.log(error)
      throw error
    }
  }

  /**
   * Delete a termscondition with id.
   * DELETE termsconditions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    try {
      const { id } = params

      const terms = await Terms.query()
        .where('id', id)
        .delete()

      response.status().send({
        message: '',
        data: terms
      })
    } catch (error) {
      console.log('destroy Terms and Conditions')
      console.log(error)
      throw error
    }
  }

  async last_terms({ request, response }) {
    try {
      const { id, version, date, description } = await Database.from('terms').select(
        'id',
        'version',
        'description'
      )
        .where('available', true)
        .groupBy('id', 'version', 'description')
        .max('created_at as date')
        .first()
      return {
        message: null,
        data: {
          id,
          version,
          date,
          description,
        }
      }
    } catch (error) {
      console.log('last terms Terms and Conditions')
      console.log(error)
      throw new GE.HttpException('Tuvimos problemas cargando los términos y condiciones de uso. Por favor intente más tarde\r(COD TERM404)', 404)
    }
  }
}

module.exports = TermsConditionController
