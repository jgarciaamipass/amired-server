'use strict'
const FirebaseSDK = require('firebase-admin')
var serviceAccount = require("../../../firebaseConfig.json");

class UserConfigController {
    async show({ request, response, auth }) {
        try {
            const { username } = request.body
            if (!FirebaseSDK.apps.length) {
                FirebaseSDK.initializeApp({
                    credential: FirebaseSDK.credential.cert(serviceAccount),
                    databaseURL: "https://amipass-2d972.firebaseio.com"
                })
            }
            return FirebaseSDK.database()
                .ref(`/users/${username}`)
                .once('value')
                .then(snapshot => {
                    return snapshot.val()
                }).catch(error => {
                    console.log('find Firebase SDK')
                    console.log(error)
                    throw error
                })
        } catch (error) {
            console.log('show User Config')
            console.log(error)
            throw error
        }
    }

    async changeEnvironment({ request, response, auth }) {
        try {
            const { environment, username } = request.body
            if (!FirebaseSDK.apps.length) {
                FirebaseSDK.initializeApp({
                    credential: FirebaseSDK.credential.cert(serviceAccount),
                    databaseURL: "https://amipass-2d972.firebaseio.com"
                })
            }
            return FirebaseSDK.database()
                .ref(`/users/${username}`)
                .update({ environment }).then(() => {
                    return {
                        message: null,
                        data: {
                            username,
                            environment
                        }
                    }
                }).catch(error => {
                    console.log('update Firebase SDK')
                    console.log(error)
                    throw error
                })
        } catch (error) {
            console.log('Change Environment User Config')
            console.log(error)
            throw error
        }
    }
}

module.exports = UserConfigController
