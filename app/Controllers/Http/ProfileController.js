'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const BOProfile = use('App/Models/BO/TbEmpleado')
const Profile = use('App/Models/Profile')
const BOAspnetMembership = use('App/Models/BO/AspnetMembership');
const BOUser = use('App/Models/BO/AspnetUser');

/**
 * Resourceful controller for interacting with profiles
 */
class ProfileController {
  /**
   * Show a list of all profiles.
   * GET profiles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response }) {
  }

  /**
   * Render a form to be used for creating a new profile.
   * GET profiles/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({ request, response }) {
  }

  /**
   * Create/save a new profile.
   * POST profiles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single profile.
   * GET profiles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response, auth }) {
    const { username, idusers } = auth.user
    const { id } = params
    const boProfile = await BOProfile.query()
      .select([
        'sEmpresa as employer',
        'sNombre as firstname',
        'sApellidoPaterno as lastname',
        'sAPellidoMaterno as lastname2',
        'sRUT as rut',
        'sEMail as email',
        'sDireccion as address',
        'sComuna as comuna',
        'sCiudad as city',
        'sRegion as region',
        // 'sTelefonoMovil as mobile',
        // 'sTelefonoFijo as phone',
        'dNacimiento as birthday',
        'sSexo as genere',
        'sEstado as state'
      ])
      .where({
        'sEmpleado': username,
        'sEmpresa': id
      })
      .first()

    const profile = await Profile.query()
      .where({
        idusers,
        employer: id
      }).first()

    return {
      message: null,
      data: {
        boProfile,
        profile
      }
    }
  }

  /**
   * Render a form to update an existing profile.
   * GET profiles/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({ params, request, response }) {
  }

  /**
   * Update profile details.
   * PUT or PATCH profiles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ request, response, auth }) {
    try {
      const { username, idusers } = auth.user

      let {
        firstname,
        lastname,
        lastname2,
        birthday,
        email,
        employer,
        avatar,
        rut
      } = request.body
      if (birthday !== null) {
        let dateSeparated = birthday.replace(/T/g, " ").split(' ')
        let separated = dateSeparated[0].replace(/\//g, "-").split('-')
        let formatted = new Date(`${Number(separated[0])}-${Number(separated[1])}-${Number(separated[2])}`)
        birthday = formatted
      }

      const boUser = await BOUser.query()
        .where('UserName', username)
        .first();

      // let boAspnetMembership = await BOAspnetMembership.query()
      //   .where('UserId', boUser['UserId'])
      //   .update({
      //     'Email': `${email}`.toUpperCase(),
      //     'LoweredEmail': `${email}`.toLowerCase()
      //   });

      let boProfile = await BOProfile.query()
        .where({
          'sEmpleado': username,
          'sEmpresa': employer
        })
        .update({
          'sNombre': firstname,
          'sApellidoPaterno': lastname,
          'sAPellidoMaterno': lastname2,
          'sEMail': email,
          // 'sTelefonoMovil': mobile,
          'dNacimiento': birthday,
        })

      let profile = await Profile.query()
        .where({
          idusers,
          employer
        })
        .update({
          idusers,
          firstname,
          lastname,
          lastname2,
          email,
          birthday,
          // telephone: mobile,
          username: `${username}`.toUpperCase(),
          employer,
          avatar
        })

      boProfile = await BOProfile.query()
        .select([
          'sEmpresa as employer',
          'sNombre as firstname',
          'sApellidoPaterno as lastname',
          'sAPellidoMaterno as lastname2',
          'sRUT as rut',
          'sEMail as email',
          'sDireccion as address',
          'sComuna as comuna',
          'sCiudad as city',
          'sRegion as region',
          // 'sTelefonoMovil as mobile',
          // 'sTelefonoFijo as phone',
          'dNacimiento as birthday',
          'sSexo as genere',
          'sEstado as state',
        ])
        .where({
          'sEmpleado': username,
          'sEmpresa': employer
        })
        .first()

      profile = await Profile.query()
        .where({
          idusers,
          employer
        }).first()

      return {
        data: {
          employer: boProfile.employer,
          firstname: boProfile.firstname,
          lastname: boProfile.lastname,
          lastname2: boProfile.lastname2,
          rut: boProfile.rut,
          email: boProfile.email,
          address: boProfile.address,
          comuna: boProfile.comuna,
          city: boProfile.city,
          region: boProfile.region,
          // mobile: boProfile.mobile,
          // phone: boProfile.phone,
          birthday: boProfile.birthday,
          genere: boProfile.genere,
          state: boProfile.state,
          avatar: profile.avatar
        }
      }
    } catch (error) {
      console.log('update Profile')
      console.log(error)
      throw error
    }
  }

  /**
   * Delete a profile with id.
   * DELETE profiles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = ProfileController
