'use strict'

const GE = require('@adonisjs/generic-exceptions')
const Hash = use('Hash')
const Database = use('Database')
const User = use('App/Models/User');
const BOUser = use('App/Models/BO/AspnetUser');
const BOAspnetMembership = use('App/Models/BO/AspnetMembership');
const BODeviceInfo = use('App/Models/BO/TbUbicacionDispositivo')
const BOProfile = use('App/Models/BO/TbEmpleado')
const Env = use('Env')
const axios = require('axios');
const { Geocoding } = require('../../../Utils/GoogleGeocodingAPI');
const ResetPassword = use('App/Models/ResetPassword');
const MailController = require('./MailController');
const Mail = use('Mail')

class UserController {
    validUsername(value) {
        return RegExp(/^[A-Z0-9]+$/i).test(value)
    }
    validPassword(password) {
        if (`${password}`.length < 4) return false
        if (`${password}`.length > 12) return false
        return true
    }
    async login({ request, response, auth }) {
        try {
            let { username, password, remember } = request.body
            username = `${username}`.toUpperCase()

            //validación de username y password
            if (!this.validUsername(username)) throw new GE.HttpException('Usuario o clave de internet inválida.\r(COD SISA404)', 404)
            if (!this.validPassword(password)) throw new GE.HttpException('Usuario o clave de internet inválida.\r(COD SISA404)', 404)

            //verificación si se encuentra y se trae la marca de bloqueado
            const userBlock = await Database.connection('mssql').raw(`
                select
                    u.UserName [username],
                    aM.IsLockedOut [blocked]
                from aspnet_Users u
                inner join aspnet_Membership aM on u.UserId = aM.UserId
                where UserName = :username
            `, {
                username
            })

            if (userBlock.length > 0) {
                if (userBlock[0].blocked) {
                    throw new GE.HttpException(`Usuario bloqueado por intentos fallidos, por favor recupere su clave presionando ¿Olvidó su clave de internet? y vuelva a ingresar.\r(COD SISA403)`, 403)
                }
            } else {
                throw new GE.HttpException('Usuario o clave de internet inválida.\r(COD SISA404)', 404)
            }

            // Valida credenciales en el BO
            const result = await axios({
                url: `${Env.get('API_V1')}/Login`,
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    sUsuario: username,
                    sClave: password
                })
            }).then((result) => {
                return result.data
            }).catch(error => {
                console.log('Login benlar User')
                console.log(error)
                throw new GE.HttpException('Usuario o clave de internet inválida.\r(COD SISB403)', 406)
            })

            //consulta si existe el usuario, lo crea en caso de no encontrarlo en pg
            const user = await User.findOrCreate(
                { 'username': username },
                {
                    username,
                    password: await Hash.make(password),
                    active: true,
                    migrated: false,
                    lastlogin: new Date(Date.now())
                }
            )

            //valida contraseñas del BO y el nuevo backend
            const valPassword = await Hash.verify(password, user.password)

            if (!valPassword) {
                user.password = password
            }
            user.lastlogin = new Date(Date.now()).toLocaleString()
            await user.save()

            //valida y crea token de autorización
            let token = null
            if (remember) {
                token = await auth
                    .withRefreshToken().attempt(username, password);
            } else {
                token = await auth.attempt(username, password);
            }

            //en caso de no ser válido genera el error
            if (!token) {
                throw new GE.HttpException('Tuvimos problemas configurando el usuario. Por favor comunicate a hola@amipass.com para solucionarlo a la brevedad.\r(COD SISA403)', 403)
            }

            //una vez validado, busca la información del usuario BO
            const boUser = await BOUser.query()
                .where('UserName', username)
                .first();
            boUser['newUser'] = (result.bUsuarioNuevo === 'True')

            return {
                message: null,
                data: {
                    ...token,
                    user,
                    boUser,
                }
            }
        } catch (error) {
            console.log('Login User')
            console.log(error)
            throw error
        }
    }

    async check({ request, response, auth }) {
        try {
            return await auth.check()
        } catch (error) {
            console.log('check User')
            console.log(error)
            return false
        }
    }

    async refresh({ request, response, auth }) {
        try {
            let { refresh_token } = request.body
            return await auth.generateForRefreshToken(refresh_token, true)
        } catch (error) {
            console.log('refresh User')
            console.log(error)
            return false
        }
    }

    async revokeUserToken({ auth }) {
        const user = auth.current.user
        const token = auth.getAuthHeader()

        await user
            .tokens()
            .where('token', token)
            .update({ is_revoked: true })
    }

    async logout({ request, response, auth }) {
        try {
            const logout = await auth.logout()
            return logout
        } catch (error) {
            console.log('logout User')
            console.log(error)
            throw error
        }
    }

    async resetPassword({ request, response, auth, view }) {
        try {
            const { username } = request.body

            let date = new Date(Date.now())

            let resetSended = await ResetPassword.query()
                .where('rut', username)
                .last()

            if (resetSended) {
                if (resetSended.expired_date < date) {
                    await resetSended.delete()
                } else {
                    let email = resetSended.email
                    let dominio = email.slice(email.indexOf("@"))
                    let tld = dominio.slice(dominio.indexOf("."))
                    throw new GE.HttpException(`Ya hemos enviado el email para que puedas cambiar la contraseña al correo ${email.charAt(0)}****${dominio.slice(dominio.indexOf("@"), 2)}****${tld}. \rVerifica tu bandeja de SPAM en caso de que no lo hayas recibido en tu bandeja de entrada.\r(COD SRCA302)`, 302)
                }
            }

            const boUser = await BOUser.query()
                .where('UserName', username)
                .first();
            if (!boUser) {
                throw new GE.HttpException('Usuario RUT o NN ingresado es inválido. \rVerifica e intenta de nuevo.\r(COD SRCA404)', 404)
            }
            const boAspnetMembership = await BOAspnetMembership.query()
                .where('UserId', boUser.UserId)
                .first()

            let email = boAspnetMembership.LoweredEmail

            let resetPassword = await axios({
                url: `${Env.get('API_V1')}/ReestablecimientoClave`,
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    sEmpleado: username,
                    sEmail: email
                })
            }).then(async response => {
                const { sCodigoGenerado } = response.data;

                if (sCodigoGenerado === '') {
                    throw new GE.HttpException('Usuario o email no encontrado.\r(COD SRCB404)', 404)
                }

                await Database
                    .table('users')
                    .where('username', username)
                    .update('password', await Hash.make(sCodigoGenerado))

                let token = await auth.attempt(username, sCodigoGenerado);
                if (!token) {
                    throw new GE.HttpException('Error al registrar el token.\r(COD SRCA400)', 400)
                }

                date.setMinutes(date.getMinutes() + 10)

                return await ResetPassword.create({
                    new_password: sCodigoGenerado,
                    expired_date: date,
                    email,
                    rut: username,
                    link: `${Env.get('APP_URL', 'http://localhost/api/v2')}/reset_password/?token=${token.token}`
                })
            }).catch((error) => {
                console.log('RestablecimientoClave benlar en User')
                console.log(error)
                if (error.response.status == 406) {
                    throw new GE.HttpException('Los datos ingresados no son correctos.\r(COD SRCB200)', error.response.status)
                }
                throw new GE.HttpException(`Por favor intente más tarde.\r(COD SRCA${error.response.status})`, error.response.status)
            })

            const { sNombre, sApellidoPaterno } = await BOProfile.query()
                .where('sEmpleado', username)
                .first();
            let data = JSON.stringify(resetPassword)

            const mailController = new MailController()
            await mailController.mailForgot({ request: { ...JSON.parse(data), name: `${sNombre} ${sApellidoPaterno}` }, response });
            return { email }
        }
        catch (error) {
            console.log('reset password User')
            console.log(error)
            throw error
        }
    }

    async checkExpiredToken({ request, response }) {
        try {
            const { token } = request.body
            let resetPassword = await ResetPassword.query()
                .where('link', 'like', `%${token}`).first()
            let validate = !(resetPassword === null)

            if (resetPassword) {
                let date = new Date(Date.now())
                validate = (date < resetPassword.expired_date)
            }
            response.send({ validate })
        }
        catch (error) {
            console.log('check expired token User')
            console.log(error)
            throw error
        }
    }

    async changePassword2({ request, response }) {
        try {
            let { password, passwordConfirm, token } = request.all()

            if (password !== passwordConfirm) {
                throw new GE.HttpException('Las claves no coinciden.\r(COD CCI2A401)', 401)
            }

            let resetPassword = await ResetPassword.query()
                .where('link', 'like', `%${token}`).first()
            let validate = !(resetPassword === null)

            if (resetPassword) {
                let date = new Date(Date.now())
                validate = (date < resetPassword.expired_date)
            }

            if (!validate) {
                throw new GE.HttpException('Datos no válidos.\r(COD CCI2A404)', 404)
            }

            let { rut, new_password, email } = resetPassword

            const result = await axios({
                url: `${Env.get('API_V1')}/CambioContraseña`,
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    sEmpleado: rut,
                    sClaveActual: new_password,
                    sClaveNueva: passwordConfirm,
                    bClaveAcceso: true,
                    bClavePago: false
                })
            }).then((response) => {
                const { bActualizada } = response.data
                return bActualizada
            }).catch((error) => {
                console.log('CambioContraseña benlar en User')
                console.log(error)
                if (error.response.status == 406) {
                    throw new GE.HttpException('Clave actual invalida ingrese nuevamente.\r(COD CCI2B403)', 403)
                }
                throw new GE.HttpException('Servicio no disponible, por favor intente más tarde.\r(COD CCI2B500)', 500)
            })

            if (result) {
                await resetPassword.delete()
                const { sNombre, sApellidoPaterno } = await BOProfile.query()
                    .where('sEmpleado', rut)
                    .first();

                await Mail.send('emails.resetPasswordSuccessfully', {
                    name: `${sNombre} ${sApellidoPaterno}`,
                    email
                }, (message) => {
                    message
                        .to(email)
                        .from('hola@amipass.com')
                        .subject('Cambio de contraseña exitoso')
                })
            }

            return {
                validate,
                changed: result
            }
        } catch (error) {
            console.log('change password 2 User')
            console.log(error)
            throw error
        }
    }

    async changePassword({ request, response }) {
        try {
            const { username, passwordOld, passwordNew, email } = request.body

            if (email == null || email == '') throw new GE.HttpException('Se requiere un correo electrónico.\r(COD CCIA403)', 403)

            const result = await axios({
                url: `${Env.get('API_V1')}/CambioContraseña`,
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    sEmpleado: username,
                    sClaveActual: passwordOld,
                    sClaveNueva: passwordNew,
                    bClaveAcceso: true,
                    bClavePago: false
                })
            }).then((response) => {
                const { bActualizada } = response.data
                return bActualizada
            }).catch((error) => {
                console.log('logout User')
                console.log(error)
                if (error.response.status == 406) {
                    throw new GE.HttpException('Clave actual invalida ingrese nuevamente.\r(COD CCIB403)', 403)
                }
                throw new GE.HttpException('Por favor intente más tarde.\r(COD CCIB500)', 500)
            })

            if (result) {
                const boUser = await BOUser.query()
                    .where('UserName', username)
                    .first();

                let boAspnetMembership = await BOAspnetMembership.query()
                    .where('UserId', boUser['UserId'])
                    .update({
                        'Email': `${email}`.toUpperCase(),
                        'LoweredEmail': `${email}`.toLowerCase()
                    });
                await BOProfile.query()
                    .where('sEmpleado', username)
                    .update({
                        'sEmail': email
                    })
            }

            return {
                message: null,
                data: {
                    changed: result
                }
            }
        } catch (error) {
            console.log('change password User')
            console.log(error)
            throw error
        }
    }

    async changeKeyPay({ request, response, auth }) {
        try {
            const { username, keyOld, keyNew } = request.body
            const result = await axios({
                url: `${Env.get('API_V1')}/CambioContraseña`,
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    sEmpleado: username,
                    sClaveActual: keyOld,
                    sClaveNueva: keyNew,
                    bClaveAcceso: false,
                    bClavePago: true
                })
            }).then((response) => {
                const { bActualizada } = response.data
                return bActualizada
            }).catch((error) => {
                console.log('CambioContraseña benlar en User')
                console.log(error)
                if (error.response.status == 406) {
                    throw new GE.HttpException('Clave actual invalida ingrese nuevamente.\r(COD CCIB403)', 403)
                }
                throw new GE.HttpException('Por favor intente más tarde.\r(COD CCIB500)', 500)
            })

            return {
                message: null,
                data: {
                    changed: result
                }
            }
        } catch (error) {
            console.log('change key pay User')
            console.log(error)
            throw error
        }
    }

    async signin({ request, response }) {
        try {
            const { username, password } = request.body;

            const boUser = await BOUser.findBy({
                UserName: username
            });

            if (boUser) {
                const user = await User.create({
                    username,
                    password
                })
                return response.status(201).send(user)
            }

            return response.status(400).send({
                message: {
                    title: 'Register user',
                    text: "User don't found"
                },
                data: null
            })
        } catch (error) {
            console.log('sign in User')
            console.log(error)
            throw error
        }
    }

    async deviceInfo({ request, response, auth }) {
        try {
            const {
                username,
                latitude,
                longitude,
                tokenFirebase,
                so,
                modelName,
                manufacturer,
                osName,
                modelId,
                CountryNetwork,
                product,
            } = request.body

            const user = await User.findBy({
                'username': username
            })

            if (tokenFirebase == null & user.tokenfirebase == null) return
            user.tokenfirebase = (tokenFirebase == null) ? user.tokenfirebase : tokenFirebase
            user.datelasttokenfirebase = new Date(Date.now())
            user.latitude = latitude
            user.longitude = longitude
            user.last_version = product
            await user.save()

            await Database
                .connection('mssql')
                .table('TbUbicacionDispositivo')
                .where('sMac', username)
                .delete()

            let deviceInfo = await BODeviceInfo.create({
                'sMac': username,
                'nLat': latitude,
                'nLng': longitude,
                'dFecha': new Date(),
                'sServiceId': user.tokenfirebase,
                'sPlataforma': so,
                'HardwareModel': modelName,
                'HardwareManufacturer': manufacturer,
                'HardwareOperatingSystem': `${osName}`.slice(0, 49),
                'HardwareOS': so,
                'HardwareDeviceId': `${modelId}`.slice(0, 49),
                'ConnectivityCellularNetworkCarrier': CountryNetwork,
                'ConnectivityInternetReachability': null,
                'ConnectivityWifiSsid': null,
                'ConnectivityIpAddress': null,
                'AppIsBackgrounded': '0',
                'BatteryPercentage': '0',
                'BatteryStatus': '0',
                'sTelefono': null,
                'sVersionApp': product
            })

            await new Geocoding().ReverseGeocoding(latitude, longitude, user.idusers)
                .then(response => {
                    return response
                })

            return {
                message: null,
                data: {
                    deviceInfo
                }
            }
        } catch (error) {
            console.log('device info User')
            console.log(error)
            throw error
        }
    }
}

module.exports = UserController
