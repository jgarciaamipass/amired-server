'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const BODynamicKey = use('App/Models/BO/TbClaveTemporal')

/**
 * Resourceful controller for interacting with paysmartdynamickeys
 */
class PaySmartDynamicKeyController {
  /**
   * Show a list of all paysmartdynamickeys.
   * GET paysmartdynamickeys
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, auth, params }) {
    try {
      const { username } = auth.user
      const { employer } = request.all()

      let date = new Date(Date.now())

      date.setHours(date.getHours() - 3);
      const boDynamicKey = await BODynamicKey.query()
        .select([
          'nClave as id',
          'sClave as code',
          'sEmpleado as username',
          'sEmpresa as employer',
          'dVencimiento as validTo',
        ])
        .where({
          'sEmpleado': username,
          'sEmpresa': employer,
          'bActiva': null,
        })
        .where('dVencimiento', '>', date)
        .orderBy('dVencimiento', 'desc')
        .fetch()
      
      response.send(boDynamicKey.rows)
    } catch (error) {
      console.log('index Pay Smart Dynamic Key')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new paysmartdynamickey.
   * GET paysmartdynamickeys/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new paysmartdynamickey.
   * POST paysmartdynamickeys
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const { username, employer } = request.body
    try {
      Date.prototype.addHours = function(h) {
        this.setTime(this.getTime() + (h*60*60*1000));
        return this;
      }

      const code = Math.floor(Math.random() * (9999 - 1000)) + 1000
      const validTo = new Date().addHours(1)

      const boDynamicKey = await BODynamicKey.create({
        'sClave': code,
        'sEmpleado': username,
        'sEmpresa': employer,
        'dVencimiento': validTo
      })

      response.status(201).send({
        data: {
          code: boDynamicKey.sClave,
          validTo: boDynamicKey.dVencimiento
        },
        message: null
      })
    } catch (error) {
      console.log('store Pay Smart Dynamic Key')
      console.log(error)
      throw error
    }
  }

  /**
   * Display a single paysmartdynamickey.
   * GET paysmartdynamickeys/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing paysmartdynamickey.
   * GET paysmartdynamickeys/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update paysmartdynamickey details.
   * PUT or PATCH paysmartdynamickeys/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a paysmartdynamickey with id.
   * DELETE paysmartdynamickeys/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = PaySmartDynamicKeyController
