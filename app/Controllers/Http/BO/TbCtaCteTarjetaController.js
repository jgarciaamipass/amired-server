'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Tarjeta = use('App/Models/BO/TbCtaCteTarjeta')

/**
 * Resourceful controller for interacting with tbctactetarjetas
 */
class TbCtaCteTarjetaController {
  /**
   * Show a list of all tbctactetarjetas.
   * GET tbctactetarjetas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, params, view }) {
    const { id } = params;
    return await Tarjeta.query().where('sTarjeta', id).fetch();
  }

  /**
   * Render a form to be used for creating a new tbctactetarjeta.
   * GET tbctactetarjetas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new tbctactetarjeta.
   * POST tbctactetarjetas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single tbctactetarjeta.
   * GET tbctactetarjetas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing tbctactetarjeta.
   * GET tbctactetarjetas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update tbctactetarjeta details.
   * PUT or PATCH tbctactetarjetas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a tbctactetarjeta with id.
   * DELETE tbctactetarjetas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = TbCtaCteTarjetaController
