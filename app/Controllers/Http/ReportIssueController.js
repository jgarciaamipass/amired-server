'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const ReportIssue = use("App/Models/ReportIssue")
const Mail = use('Mail')

/**
 * Resourceful controller for interacting with reportissues
 */
class ReportIssueController {
  /**
   * Show a list of all reportissues.
   * GET reportissues
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new reportissue.
   * GET reportissues/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new reportissue.
   * POST reportissues
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    try {
      let { idusers } = auth.user
      const { user, device, cellular, comment, reason } = request.body
      const issue = await ReportIssue.create({
        user_info: user,
        device,
        cellular,
        date: new Date(Date.now()),
        comment,
        idusers,
        reason
      })
      const mail = await Mail.send('emails.reportIssue', issue.toJSON(), (message) => {
        message
          .to('hola@amipass.com')
          .from('hola@amipass.com')
          .cc('ti@amipass.com')
          .subject('Issue Report: ' + issue.idreport_issue)
      })
      response.send({
        send: true
      })
    } catch (error) {
      console.log('report Issue Mail')
      console.log(error)
      throw error
    }
  }

  /**
   * Display a single reportissue.
   * GET reportissues/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ request, response, auth }) {
    try {
      const { idusers } = auth.user
      return await ReportIssue.query().where('idusers', idusers).fetch();
    } catch (error) {
      throw error
    }
  }

  /**
   * Render a form to update an existing reportissue.
   * GET reportissues/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update reportissue details.
   * PUT or PATCH reportissues/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a reportissue with id.
   * DELETE reportissues/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = ReportIssueController
