'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const GE = require('@adonisjs/generic-exceptions')
const UserTerms = use('App/Models/UserTerm');
const Terms = use('App/Models/Term');
const Database = use('Database');

/**
 * Resourceful controller for interacting with userterms
 */
class UserTermsController {
  /**
   * Show a list of all userterms.
   * GET userterms
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  async last({ request, response, view, auth }) {
    try {
      const { idusers } = auth.user
      const { id, version, date } = await Database.from('terms').select(
        'id',
        'version'
      )
        .where('available', true)
        .groupBy('id', 'version')
        .max('created_at as date')
        .first()

      const userTerms = await UserTerms.query().where('user_id', idusers).last()

      if (userTerms) {
        return {
          isUpdated: (id === userTerms.terms_id),
        }
      } else {
        return {
          isUpdated: false,
        }
      }
    } catch (error) {
      console.log('last User Terms Conditions')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new userterm.
   * GET userterms/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new userterm.
   * POST userterms
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    const { username, idusers } = auth.user
    const { user, accept } = request.body

    const { id } = await Database.from('terms').select(
      'id',
      'version'
    )
      .where('available', true)
      .groupBy('id', 'version')
      .max('created_at as date')
      .first()

    if (user === username) {
      const usetTerms = await UserTerms.create({
        terms_id: id,
        user_id: idusers,
        accept
      })
      response.status(201).send({
        message: '',
        data: {
          accepted: accept
        }
      })
    } else {
      throw new GE.HttpException('El usuario no corresponde a la autorización.\r(COD TERMSA403)', 403)
    }
  }

  /**
   * Display a single userterm.
   * GET userterms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing userterm.
   * GET userterms/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update userterm details.
   * PUT or PATCH userterms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {

  }

  /**
   * Delete a userterm with id.
   * DELETE userterms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = UserTermsController
