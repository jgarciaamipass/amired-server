'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Database = use('Database');
const BOCtaCteTarjeta = use('App/Models/BO/TbCtaCteTarjeta');
const BOCards = use('App/Models/BO/TbTarjeta');
const Transactions = use('App/Models/Transaction')

/**
 * Resourceful controller for interacting with statementaccounts
 */
class StatementAccountController {
  /**
   * Show a list of all statementaccounts.
   * GET statementaccounts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { card, filter } = request.body;

    const rows = await BOCtaCteTarjeta.query()
      .select([
        'iCorrelativo as id'
      ])
      .where('sTarjeta', card)
      .orderBy('iCorrelativo', 'desc')
      .fetch();

    const rowsIndex = rows.toJSON().map(r => r.id);
    let rowInt = (filter.page - 1) * filter.size;
    let rowEnd = filter.page * filter.size;

    const indx = rowsIndex.slice(rowInt, rowEnd);

    const transactions = await BOCtaCteTarjeta.query()
      .select([
        'iCorrelativo as id',
        'dMovimiento as date',
        'nMonto as amount',
        'sTipoMovimiento as type',
        'nAsignacion as idassig',
        'nTransaccion as transact'
      ])
      .whereIn('iCorrelativo', indx)
      .orderBy('iCorrelativo', 'desc')
      .fetch()

    return {
      message: null,
      data: transactions
    }
  }

  /**
   * Render a form to be used for creating a new statementaccount.
   * GET statementaccounts/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new statementaccount.
   * POST statementaccounts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single statementaccount.
   * GET statementaccounts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, auth }) {
    try {
      const { username } = params
      const { card, filters } = request.body
      let { size, page, dateInit, dateEnd, typeOper } = (typeof filters !== 'undefined') ? filters :
        {
          size: 10,
          page: 1,
          dateInit: null,
          dateEnd: null,
          typeOper: null
        }
      console.log(request.body)
      switch (typeOper) {
        case 'TRA':
          typeOper = 'TD'
          break;
        case 'PAY':
          typeOper = 'TX'
          break;
        case 'DEP':
          typeOper = 'AS'
          break;
        default:
          typeOper = null
          break;
      }

      let dInit = (dateInit == null) ? null : new Date(dateInit).toISOString().replace(/([^T]+)T([^\.]+).*/g, '$1 00:00:00')
      let dEnd = (dateEnd == null) ? null : new Date(dateEnd).toISOString().replace(/([^T]+)T([^\.]+).*/g, '$1 23:59:59')

      const infoCard = await BOCards.query().select([
        'sEmpleado as employee',
        'sEmpresa as employer'
      ])
        .where('sTarjeta', card)
        .first()

      const cards = await BOCards.query().select([
        'sTarjeta as card'
      ])
        .where({
          'sEmpleado': infoCard.employee,
          'sEmpresa': infoCard.employer
        })
        .fetch()

      const inCars = await cards.rows.map((item) => {
        return `'${item.card}'`
      })
      const cardsParams = `(${inCars})`.replace('[', '').replace(']', '')

      const transactions = await Database.connection('mssql').raw(`
        ;WITH PageNumbers AS(
              SELECT ROW_NUMBER() OVER(ORDER BY iCorrelativo desc) row,
                    mov.iCorrelativo as idtransaction,
                    CASE mov.sTipoMovimiento
                        WHEN 'TX' THEN mov.nTransaccion
                        WHEN 'TO' THEN tso.ID
                        WHEN 'TD' THEN tsd.ID
                        WHEN 'AS' THEN mov.nAsignacion
                        WHEN 'AN' THEN mov.nTransaccion
                    END as number_operation,
                    CASE mov.sTipoMovimiento
                        WHEN 'TX' THEN 'PAY'
                        WHEN 'TO' THEN 'TRN'
                        WHEN 'TD' THEN 'TRP'
                        WHEN 'AS' THEN 'DEP'
                        WHEN 'AN' THEN 'ANU'
                    END as type_operation,
                    mov.dMovimiento as date,
                    isnull(CASE mov.sTipoMovimiento
                        WHEN 'AN' THEN 1
                    END, 0) as voided,
                    '' as description,
                    isnull(CASE mov.sTipoMovimiento
                        WHEN 'TX' THEN mov.nMonto
                        WHEN 'TO' THEN mov.nMonto
                        WHEN 'AN' THEN mov.nMonto
                    END, 0) as debit,
                    isnull(CASE mov.sTipoMovimiento
                        WHEN 'TD' THEN mov.nMonto
                        WHEN 'AS' THEN mov.nMonto
                        WHEN 'AN' THEN mov.nMonto
                    END, 0) as accredit,
                    CASE mov.sTipoMovimiento
                        WHEN 'TX' THEN cast(lo.sDescripcion as varchar(120))
                        WHEN 'TO' THEN empleDes.sNombre + ' ' + empleDes.sApellidoPaterno + ' ' + empleDes.sAPellidoMaterno
                        WHEN 'TD' THEN empleOrg.sNombre + ' ' + empleOrg.sApellidoPaterno + ' ' + empleOrg.sAPellidoMaterno
                        WHEN 'AS' THEN empre.sRazonSocial
                        WHEN 'AN' THEN 'ANULADO'
                    END [from],
                    CASE mov.sTipoMovimiento
                      WHEN 'TX' THEN tra.sEstablecimiento + tra.sLocal --upay.sUsuario
                      WHEN 'TO' THEN tarDes.sTarjeta
                      WHEN 'TD' THEN tarOrg.sTarjeta
                      WHEN 'AS' THEN empre.sEmpresa
                      WHEN 'AN' THEN 'ANULADO'
                    END [account]
          from TbCtaCteTarjetas mov
              left join TbLogTraspasoSaldos tso on tso.nCtaCteTarjetaOrigen = mov.iCorrelativo
              left join TbLogTraspasoSaldos tsd on tsd.nCtaCteTarjetaDestino = mov.iCorrelativo
              left join TbTransacciones tra on tra.idTransaccion = mov.nTransaccion
              left join TbAsignaciones asg on asg.nAsignacion = mov.nAsignacion

              left join TbTarjetas tarDes on tso.sTarjetaDestino = tarDes.sTarjeta
              left join TbTarjetas tarOrg on tsd.sTarjetaOrigen = tarOrg.sTarjeta
              left join TbEmpleados empleDes on tarDes.sEmpresa = empleDes.sEmpresa and tarDes.sEmpleado = empleDes.sEmpleado
              left join TbEmpleados empleOrg on tarOrg.sEmpresa = empleOrg.sEmpresa and tarOrg.sEmpleado = empleOrg.sEmpleado

              left join TbLocales lo on tra.sEstablecimiento = lo.sEstablecimiento and tra.sLocal = lo.sLocal

              left join TbEmpresas empre on asg.sEmpresa = empre.sEmpresa
          where
            mov.sTarjeta in ${cardsParams}
            and (((:dateInit is null) or (:dateEnd is null)) or (((:dateInit is not null) and (:dateEnd is not null)) and mov.dMovimiento between :dateInit and :dateEnd))
            and ((:typeOper is null) or ((:typeOper is not null) and (mov.sTipoMovimiento = :typeOper)))
        )
        SELECT  *
        FROM    PageNumbers
        WHERE   row  BETWEEN ((:page - 1) * :size + 1)
                AND (:page * :size)
        `,
        {
          size,
          page,
          dateInit: dInit,
          dateEnd: dEnd,
          typeOper
        })

      const result = transactions.map(async (item) => {
        const row = await Transactions.query()
          .where({
            'number_operation': item.number_operation,
            'type_operation': item.type_operation
          }).first()

        item['comment'] = (row !== null) ? row.comment : null
        return await item
      })

      return {
        message: null,
        data: await Promise.all(result)
      }
    } catch (error) {
      console.log('show Statement Account')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to update an existing statementaccount.
   * GET statementaccounts/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update statementaccount details.
   * PUT or PATCH statementaccounts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a statementaccount with id.
   * DELETE statementaccounts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  async balance({ request, response, view }) {
    try {
      const { username, employer } = request.body
      const balance = await BOCtaCteTarjeta
        .query()
        .leftJoin('TbTarjetas', 'TbCtaCteTarjetas.sTarjeta', 'TbTarjetas.sTarjeta')
        .where({
          'sEmpleado': username,
          'sEmpresa': employer
        })
        .sum('nMonto as balance')
        .first()

      if (balance.balance == null) {
        balance.balance = 0
      }
      return {
        message: null,
        data: balance
      }
    } catch (error) {
      console.log('balance Statement Account')
      console.log(error)
      throw error;
    }
  }
}

module.exports = StatementAccountController
