'use strict'

const { AzureStorageBlob } = require('../../../Utils/AzureStorageBlob')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with products
 */

const Drive = use('Drive')
const Database = use('Database')
const getStream = require('into-stream');

class ProductController {
  /**
   * Show a list of all products.
   * GET products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response }) {
    try {
      const { latitude, longitude, filters } = request.body
      const { size, page, tag, recent } = (typeof filters !== 'undefined') ? filters :
        {
          size: 10,
          page: 1,
          tag: null,
          recent: false
        }
      const promotions = await Database.connection('mssql').raw(`
      ;WITH PageNumbers AS (
        SELECT ROW_NUMBER() OVER(ORDER BY dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude)) row,
              pro.idPromocion code,
              pro.sEstablecimiento commerce,
              pro.sLocal store,
              pro.sTipo category,
              pro.sCodigoPromocion codePromotion,
              pro.dInicio dateInit,
              pro.dTermino dateEnf,
              pro.nMontoPreferencial amountPreference,
              pro.nMontoReal amount,
              pro.nDescuento discount,
              pro.nCuponesDisponibles availableCoupons,
              pro.sDashTitulo title,
              pro.sDescripcionMovil description,
              pro.bLunes monday,
              pro.bMartes tuesday,
              pro.bMiercoles wednesday,
              pro.bJueves thursday,
              pro.bViernes friday,
              pro.bViernes frisday,
              pro.bSabado saturday,
              pro.bDomingo sunday,
              pro.sCondiciones conditions,
              pro.bDescuentoGeneral gemeralDiscount,
              pro.bDescuentoAmipass amipassDiscount,
              iDashFotoGrande as imageLarge,
              upay.sUsuario userPay,
              e.sRut rut,
              e.sRazonSocial name,
              l.sDescripcion alias,
              l.sDireccion + ' ' + ISNULL(l.sDireccionComplemento, '') address,
              l.nLat latitude,
              l.nLng longitude,
              dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude) distance,
              l.bLogo logoActive,
              pho.iFotoChica as logo,
              co.sDescripcion as comuna,
              re.sDescripcion as region,
              l.bAppPAY payApp,
              l.bSmartphoneIntegrado as payQR,
              l.bSmartphone as payCC,
              l.bH2H paykDyn,
              l.bPosGrps payCard,
              l.bPosEthernet payCard2,
              l.sContactoTelefono telContact,
              l.sTelefonoLocal telStore,
              l.sContactoMovil telMobile
            FROM
                TbPromocionesNuevas pro
                left join TbLocales l on pro.sEstablecimiento = l.sEstablecimiento and ((pro.sLocal = l.sLocal) or (pro.sLocal = 'Todos'))
                INNER JOIN TbEstablecimientos AS e ON l.sEstablecimiento = e.sEstablecimiento
                left join TbRegiones re on l.sRegion = re.sRegion
                left join TbComunas co on co.sComuna = l.sComuna
                left join TbUsuariosPay upay on l.sEstablecimiento = upay.sEstablecimiento and l.sLocal = upay.sLocal
                left join TbLocalesFotos pho on l.sEstablecimiento = pho.sEstablecimiento and l.sLocal = pho.sLocal
              WHERE (l.sEstado = 'AC')
                and pro.bAprobacionAmipass = 1
                AND (dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude) between 0 and 5)
                and bActivarMapa = 1
                and pro.sTipo <> 'DescuentoSKU'
                and cast(getdate() as date) between cast(pro.dInicio as date) and cast(pro.dTermino as date)
            )
            SELECT  *
            FROM    PageNumbers
            WHERE   row  BETWEEN ((:page - 1) * :size + 1)
                    AND (:page * :size)
      `, {
        latitude: `${latitude}`,
        longitude: `${longitude}`,
        size,
        page,
        recent: (typeof recent === 'undefined') ? null : recent,
      })

      let result = promotions.map(async item => {

        try {
          this.uploadImageAzure({ params: {code: item.code }})
        } catch (error) {
          console.log('upload image in index product')
          console.log(error)
        }

        try {
          if (item.imageLarge !== null) {
            await Drive.put(`commerces/${item.commerce}/${item.store}/promotion/${item.code}.png`, item.imageLarge)
          }
        } catch (error) {
          console.log('Creando imágen de la promoción')
          console.log(error)
        }

        const boTags = await Database.connection('mssql').raw(`
					select 
              sClasificacion [tag],
              dom.sDescripcion [name]
					from TbLocalesClasificacion cla
          inner join TbDominios dom on dom.sCodigo = cla.sClasificacion and dom.sDominio = 'CLASIFICACION_LOCAL'
					where sEstablecimiento = :commerce and sLocal = :store
					`, {
          commerce: item.commerce,
          store: item.store
        })
        if (boTags.length > 0) {
          try {
            item['tags'] = boTags
            item['tagCode'] = boTags[0].tag
            item['tag'] = boTags[0].name
          } catch (error) {
            console.log('Clasificación en Product')
            console.log(error)
          }
        } else {
          item['tags'] = [{ tag: null, name: 'Sin Categoría' }]
          item['tagCode'] = null
          item['tag'] = 'Sin Categoría'
        }

        return await item
      })

      const data = await Promise.all(result.map((element, index) => {
        return element
      }))

      return {
        message: {
          label: null,
          text: null
        },
        data
      }
    } catch (error) {
      console.log('Index Product')
      console.log(error)
      throw error
    }
  }

  async list({ request, response }) {
    try {
      const { latitude, longitude, filters } = request.body
      const { size, page, tag, recent } = (typeof filters !== 'undefined') ? filters :
        {
          size: 10,
          page: 1,
          tag: null,
          recent: false
        }
      const result = await Database.connection('mssql').raw(`
      ;WITH PageNumbers AS (
        SELECT ROW_NUMBER() OVER(ORDER BY dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude)) row,
          pro.idPromocion code,
          pro.nMontoPreferencial amountPreference,
          pro.nMontoReal amount,
          pro.nDescuento discount,
          pro.sDashTitulo title,
          l.sDescripcion alias,
          co.sDescripcion as comuna,
          re.sDescripcion as region,
          l.bAppPAY payApp,
          l.bSmartphoneIntegrado as payQR,
          l.bSmartphone as payCC,
          l.bH2H paykDyn,
          l.bPosGrps payCard,
          l.bPosEthernet payCard2
        FROM
            TbPromocionesNuevas pro
            left join TbLocales l on pro.sEstablecimiento = l.sEstablecimiento and ((pro.sLocal = l.sLocal) or (pro.sLocal = 'Todos'))
            INNER JOIN TbEstablecimientos AS e ON l.sEstablecimiento = e.sEstablecimiento
            left join TbRegiones re on l.sRegion = re.sRegion
            left join TbComunas co on co.sComuna = l.sComuna
            left join TbUsuariosPay upay on l.sEstablecimiento = upay.sEstablecimiento and l.sLocal = upay.sLocal
            left join TbLocalesFotos pho on l.sEstablecimiento = pho.sEstablecimiento and l.sLocal = pho.sLocal
          WHERE (l.sEstado = 'AC')
            and pro.bAprobacionAmipass = 1
            and bActivarMapa = 1
            AND (dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude) between 0 and 5)
            and cast(getdate() as date) between cast(pro.dInicio as date) and cast(pro.dTermino as date)
        )
        SELECT  *
        FROM    PageNumbers
        WHERE   row  BETWEEN ((:page - 1) * :size + 1)
                AND (:page * :size)
      `, {
        latitude: `${latitude}`,
        longitude: `${longitude}`,
        size,
        page,
        recent: (typeof recent === 'undefined') ? null : recent,
      })
      return {
        message: {
          label: null,
          text: null
        },
        data: result
      }
    } catch (error) {
      console.log('List Product')
      console.log(error)
      throw error
    }
  }

  async listAmibox({ request, response }) {
    try {
      const { latitude, longitude, filters } = request.body
      const { size, page } = (typeof filters !== 'undefined') ? filters :
        {
          size: 10,
          page: 1
        }
      const result = await Database.connection('mssql').raw(`
      ;WITH PageNumbers AS (
        SELECT ROW_NUMBER() OVER(ORDER BY dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude)) row,
            pro.idPromocion code,
            l.sEstablecimiento,
            l.sLocal,
            pro.sDashTitulo title,
            pro.sCondiciones,
            pro.sDescripcionMovil,
            l.sDescripcion alias,
            l.bAppPAY payApp,
            l.bSmartphoneIntegrado as payQR,
            l.bSmartphone as payCC,
            l.bH2H paykDyn,
            l.bPosGrps payCard,
            l.bPosEthernet payCard2
        FROM
            TbPromocionesNuevas pro
            inner join TbLocales l on pro.sEstablecimiento = l.sEstablecimiento and ((pro.sLocal = l.sLocal) or (pro.sLocal = 'Todos'))
            inner join TbEstablecimientos AS e ON l.sEstablecimiento = e.sEstablecimiento
        WHERE (l.sEstado = 'AC') and
            pro.bAprobacionAmipass = 1
            and bActivarMapa = 1
            and pro.sTipo = 'DescuentoSKU'
            and cast(getdate() as date) between cast(pro.dInicio as date) and cast(pro.dTermino as date)
            --AND (dbo.fnDistance(l.nLat, l.nLng, :latitude, :longitude) between 0 and 10)
        )
        SELECT  *
        FROM    PageNumbers
        WHERE   row  BETWEEN ((:page - 1) * :size + 1)
                AND (:page * :size)
      `, {
        latitude: `${latitude}`,
        longitude: `${longitude}`,
        size,
        page
      })
      return {
        message: {
          label: null,
          text: null
        },
        data: result
      }
    } catch (error) {
      console.log('ListAmibox Product')
      console.log(error)
      throw error
    }
  }

  async amibox({ request, response }) {
    try {
      const { promotion } = request.params

      const amibox = await Database.connection('mssql').raw(`
      SELECT
            pro.idPromocion code,
            l.sEstablecimiento,
            l.sLocal,
            pro.sDashTitulo title,
            pro.sCondiciones,
            pro.sDescripcionMovil,
            l.sDescripcion alias,
            l.bAppPAY payApp,
            l.bSmartphoneIntegrado as payQR,
            l.bSmartphone as payCC,
            l.bH2H paykDyn,
            l.bPosGrps payCard,
            l.bPosEthernet payCard2
        FROM
            TbPromocionesNuevas pro
            inner join TbLocales l on pro.sEstablecimiento = l.sEstablecimiento and ((pro.sLocal = l.sLocal) or (pro.sLocal = 'Todos'))
            inner join TbEstablecimientos AS e ON l.sEstablecimiento = e.sEstablecimiento
        WHERE (l.sEstado = 'AC') and
            pro.bAprobacionAmipass = 1
            and bActivarMapa = 1
            and pro.sTipo = 'DescuentoSKU'
            and pro.idPromocion = :promotion
      `, {
        promotion
      })

      const details = await Database.connection('mssql').raw(`
      select
            sDescripcion name,
            nCosto quantity,
            sUnidad unit,
            nPrecioventa price
      from TbPromocionesNuevasSKU psku
      where idPromocion = :promotion;
      `, {
        promotion
      })
      return {
        message: null,
        data: {
          promotion: amibox[0],
          details: details
        }
      }
    } catch (error) {
      console.log('amibox Product')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new product.
   * GET products/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({ request, response }) {

  }

  /**
   * Create/save a new product.
   * POST products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {

  }

  /**
   * Display a single product.
   * GET products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {
    try {
      const { code } = params
      const result = await Database.connection('mssql').raw(`
      SELECT 
        pro.idPromocion code,
        pro.sEstablecimiento commerce,
        pro.sLocal store,
        pro.sTipo category,
        pro.sCodigoPromocion codePromotion,
        pro.dInicio dateInit,
        pro.dTermino dateEnf,
        pro.nMontoPreferencial amountPreference,
        pro.nMontoReal amount,
        pro.nDescuento discount,
        pro.nCuponesDisponibles availableCoupons,
        pro.sDashTitulo title,
        pro.sDescripcionMovil description,
        pro.bLunes monday,
        pro.bMartes tuesday,
        pro.bMiercoles wednesday,
        pro.bJueves thursday,
        pro.bViernes firsday,
        pro.bSabado saturday,
        pro.bDomingo sunday,
        pro.sCondiciones conditions,
        pro.bDescuentoGeneral gemeralDiscount,
        pro.bDescuentoAmipass amipassDiscount,
        iDashFotoGrande as imageLarge,
        upay.sUsuario userPay,
        e.sRut rut,
        e.sRazonSocial name,
        l.sDescripcion alias,
        l.sDireccion + ' ' + ISNULL(l.sDireccionComplemento, '') address,
        l.nLat latitude,
        l.nLng longitude,
        l.gGeoUbicacion.STDistance(geography::STGeomFromText('POINT(' + :latitude + ' ' + :longitude + ')', 4326)) distance,
        do.sDescripcion tag,
        l.bLogo logoActive,
        'https://map.amipass.cl/image.aspx/*t=L&id='+l.sEstablecimiento+'&id2='+l.sLocal as logo,
        l.bAppPAY payApp,
        l.bSmartphoneIntegrado as payQR,
        l.bSmartphone as payCC,
        l.bH2H paykDyn,
        l.bPosGrps payCard,
        l.bPosEthernet payCard2,
        l.sContactoTelefono telContact,
        l.sTelefonoLocal telStore,
        l.sContactoMovil telMobile
      FROM
          TbPromocionesNuevas pro
          left join TbLocales l on pro.sEstablecimiento = l.sEstablecimiento and pro.sLocal = l.sLocal
          INNER JOIN TbEstablecimientos AS e ON l.sEstablecimiento = e.sEstablecimiento
          left join TbRegiones re on l.sRegion = re.sRegion
          left join TbComunas co on co.sComuna = l.sComuna
          left join TbDominios do on do.sCodigo = l.sClasificacion and sDominio = 'CLASIFICACION_LOCAL'
          left join TbUsuariosPay upay on l.sEstablecimiento = upay.sEstablecimiento and l.sLocal = upay.sLocal
          left join TbLocalesFotos pho on l.sEstablecimiento = pho.sEstablecimiento and l.sLocal = pho.sLocal
        WHERE (l.sEstado = 'AC')
          and pro.bAprobacionAmipass = 1
          AND idPromocion = :code
      `, {
        code
      })
      return {
        message: null,
        data: result
      }
    } catch (error) {
      console.log('show Product')
      console.log(error)
      throw error
    }
  }

  /**
   * Display a single product.
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async showinstore({ params, request, response }) {
    try {
      const { commerce, store } = params;

      const result = await Database.connection('mssql').raw(`
      select
            idPromocion code,
            sEstablecimiento commerce,
            sLocal store,
            sTipo category,
            sCodigoPromocion codePromotion,
            dInicio dateInit,
            dTermino dateEnf,
            nMontoPreferencial amountPreference,
            nMontoReal amount,
            nDescuento discount,
            nCuponesDisponibles availableCoupons,
            sDashTitulo title,
            sDescripcionMovil description,
            bLunes monday,
            bMartes tuesday,
            bMiercoles wednesday,
            bJueves thursday,
            bViernes frisday,
            bViernes friday,
            bSabado saturday,
            bDomingo sunday,
            sCondiciones conditions,
            bDescuentoGeneral gemeralDiscount,
            bDescuentoAmipass amipassDiscount,
            -- convert(varchar(max),convert(varbinary(max), iDashFotoChica)) as imageSmall,
            iDashFotoGrande as imageLarge
      from TbPromocionesNuevas pro
      where
            sEstablecimiento = :commerce
        and (sLocal = :store or sLocal = 'Todos')
        and bAprobacionAmipass = :authorized
        and cast(getdate() as date) between cast(dInicio as date) and cast(dTermino as date)
        and sTipo <> 'DescuentoSKU'
      `, {
        commerce,
        store,
        authorized: true
      })

      return {
        message: null,
        data: result
      }
    } catch (error) {
      console.log('show in store Product')
      console.log(error)
    }
  }

  async showAmiboxInStore({ params, request, response }) {
    try {
      const { commerce, store } = params;
      const result = await Database.connection('mssql').raw(`
      select
            idPromocion code,
            sEstablecimiento commerce,
            sLocal store,
            sTipo category,
            sCodigoPromocion codePromotion,
            dInicio dateInit,
            dTermino dateEnf,
            nMontoPreferencial amountPreference,
            nDescuento discount,
            nCuponesDisponibles availableCoupons,
            sDashTitulo title,
            sDescripcionMovil description,
            bLunes monday,
            bMartes tuesday,
            bMiercoles wednesday,
            bJueves thursday,
            bViernes frisday,
            bViernes friday,
            bSabado saturday,
            bDomingo sunday,
            sCondiciones conditions,
            bDescuentoGeneral gemeralDiscount,
            bDescuentoAmipass amipassDiscount,
            iDashFotoGrande as imageLarge,
            (select sum(nPrecioventa) from TbPromocionesNuevasSKU where idPromocion = pro.idPromocion ) as amount
      from TbPromocionesNuevas pro
      where
            sEstablecimiento = :commerce
        and (sLocal = :store or sLocal = 'Todos')
        and bAprobacionAmipass = :authorized
        and cast(getdate() as date) between cast(dInicio as date) and cast(dTermino as date)
        and sTipo = 'DescuentoSKU'
      `, {
        commerce,
        store,
        authorized: true
      })

      let promotions = await Promise.all(result.map(async promotion => {
        const row = await Database.connection('mssql').raw(`
          select
                sDescripcion name,
                nCosto quantity,
                sUnidad unit,
                nPrecioventa price
          from TbPromocionesNuevasSKU psku
          where idPromocion = :code;
        `, {
          code: promotion.code
        })
        promotion['detail'] = row
        return await promotion
      }))
        .then(result => {
          return result
        })
      return {
        message: null,
        data: promotions
      }
    } catch (error) {
      console.log('show amibox in store Product')
      console.log(error)
    }
  }

  /**
   * Render a form to update an existing product.
   * GET products/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({ params, request, response }) {

  }

  /**
   * Update product details.
   * PUT or PATCH products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {

  }

  /**
   * Delete a product with id.
   * DELETE products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {

  }

  /**
   *
   */
  async uploadImageAzure({ params, request, response }) {
    try {
      const { code } = params;
      const result = await Database.connection('mssql').raw(`
        select
              idPromocion code,
              sEstablecimiento commerce,
              sLocal store,
              iDashFotoGrande as imageLarge
        from TbPromocionesNuevas pro
        where
              idPromocion = :code
        `, {
        code
      })

      if (result[0].imageLarge !== null) {
        let { commerce, store, imageLarge } = result[0]
        await Drive.put(`commerces/${commerce}/${store}/promotion/${code}.png`, imageLarge)

        const containerClient = new AzureStorageBlob().AzureStorageClient()

        const blobName = `${commerce}/${store}/promotion/${code}.png`;
        const stream = getStream(await Drive.get(`commerces/${commerce}/${store}/promotion/${code}.png`));
        const blockBlobClient = containerClient.getBlockBlobClient(blobName);

        const ONE_MEGABYTE = 1024 * 1024;

        await blockBlobClient.uploadStream(
          stream,
          4 * ONE_MEGABYTE,
          20,
          { blobHTTPHeaders: { blobContentType: "image/*" } }
        );
        return { saved: true }
      } else {
        return { saved: false }
      }
    } catch (error) {
      console.log('upload image azure Product')
      console.log(error)
      throw error
    }
  }
}

module.exports = ProductController
