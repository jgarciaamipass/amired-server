'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const ResetPassword = use('App/Models/ResetPassword');

/**
 * Resourceful controller for interacting with logins
 */
class LoginController {
  /**
   * Show a list of all logins.
   * GET logins
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    try {
      return view.render('login.index')
    } catch (error) {
      response.send(error)
    }
  }

  async resetPassword({ request, response, view }) {
    try {
      let { token } = request.all()
      let resetPassword = await ResetPassword.query()
        .where('link', 'like', `%${token}`).first()

      let validate = !(resetPassword === null)

      if (resetPassword) {
        let date = new Date(Date.now())
        validate = (date < resetPassword.expired_date)
      }

      return view.render('login.resetPassword', {
        validate: `${validate}`,
        token
      })

    } catch (error) {
      console.log('reset password Login')
      console.log(error)
    }
  }

  async resetPasswordExpired({ request, response, view }) {
    try {
      return view.render('login.resetPasswordExpired')
    } catch (error) {
      console.log('reet password expired Login')
      console.log(error)
      response.send(error)
    }
  }

  /**
   * Create/save a new login.
   * POST logins
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single login.
   * GET logins/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing login.
   * GET logins/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update login details.
   * PUT or PATCH logins/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a login with id.
   * DELETE logins/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = LoginController
