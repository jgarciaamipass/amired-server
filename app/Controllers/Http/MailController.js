'use strict'

const ReportIssue = use("App/Models/ReportIssue")
const Mail = use('Mail')

class MailController {

    async index({ request, response, view }) {
        try {
            console.log(request)
            return view.render('emails.sendResetPassword', request)
        } catch (error) {
            console.log('index Mail')
            console.log(error)
            response.send(error)
        }
    }

    async reportIssue({ request, response, auth }) {
        try {
            let { idusers } = auth.user
            const { user, device, cellular, comment, reason } = request.body
            const issue = await ReportIssue.create({
                user_info: user,
                device,
                cellular,
                date: new Date(Date.now()),
                comment,
                idusers,
                reason
            })
            const mail = await Mail.send('emails.reportIssue', issue.toJSON(), (message) => {
                message
                    .to('hola@amipass.com')
                    .from('hola@amipass.com')
                    .cc('ti@amipass.com')
                    .subject('Issue Report: ' + issue.idreport_issue)
            })
            // console.log(mail);
            response.send({
                send: true
            })
        } catch (error) {
            console.log('report Issue Mail')
            console.log(error)
            throw error
        }
    }

    async mailForgot({ request, response }) {
        try {
            const { link, email, rut, name } = request
            const datos = {
                link,
                email,
                username: rut,
                name
            }

            const mail = await Mail.send('emails.sendResetPassword', datos, (message) => {
                message
                    .to(email)
                    .from('hola@amipass.com')
                    .subject('Restablecer contraseña')
            })
        } catch (error) {
            console.log('mail forgot Mail')
            console.log(error)
            throw error
        }
    }
}

module.exports = MailController
