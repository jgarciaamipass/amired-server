'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Communicate = use('App/Models/Communicate')
const CommunicateSeen = use('App/Models/CommunicateSeen')
const User = use('App/Models/User');

/**
 * Resourceful controller for interacting with communications
 */
class CommunicationController {
  /**
   * Show a list of all communications.
   * GET communications
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({ request, response }) {
    try {
      const communicate = await Communicate.all()
      return {
        message: null,
        data: communicate
      }
    } catch (error) {
      console.log('index Communication')
      console.log(error)
      throw error
    }
  }

  /**
   * Render a form to be used for creating a new communication.
   * GET communications/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async create({ request, response }) {

  }

  /**
   * Create/save a new communication.
   * POST communications
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    try {
      const { title, description, path, active } = request.body

      await Communicate
        .query()
        .where('active', true)
        .update({ active: false })

      const communicate = await Communicate.create({
        title,
        description,
        path,
        active
      })

      return {
        message: null,
        data: communicate
      }
    } catch (error) {
      console.log('store Communication')
      console.log(error)
      throw error
    }
  }

  /**
   * Display a single communication.
   * GET communications/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({ params, request, response }) {
  }

  /**
   * Render a form to update an existing communication.
   * GET communications/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async edit({ params, request, response }) {
  }

  /**
   * Update communication details.
   * PUT or PATCH communications/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a communication with id.
   * DELETE communications/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  async published({ request, response }) {
    try {
      const communicate = await Communicate.query()
        .where({ active: true })
        .first()

      if (communicate) {
        const { idcommunicate, title, description, path, active } = communicate
        response.status(200).send({
          _id: idcommunicate,
          title,
          description,
          path,
          active
        })
      } else {
        response.status(404).send({})
      }

    } catch (error) {
      console.log('published Communication')
      console.log(error)
      throw error
    }
  }

  async seen({ params, request, response }) {
    try {
      const { username, id, readed } = request.body

      const { idusers } = await User
        .query()
        .where({ username })
        .first()

      const communicateSeen = await CommunicateSeen
        .query()
        .where({ idcommunicate: id, showed: true, idusers })
        .first()

      if (communicateSeen === null && !readed) {
        return response.send(false)
      }

      if (communicateSeen === null && readed) {
        await CommunicateSeen.create({
          idcommunicate: id,
          idusers,
          showed: true
        })
        return response.send(true)
      }
      return response.send(true)
    } catch (error) {
      console.log('seen Communication')
      console.log(error)
      throw error
    }
  }
}

module.exports = CommunicationController
