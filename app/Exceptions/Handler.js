'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle (error, { request, response }) {
    console.log(error);
    if(error.status === 401) {
      return response.status(error.status).json({
        message: {
          title: 'Sesión expirada',
          text: 'Tu sesión ha expirado. Para continuar debes iniciar sesión nuevamente.\r(COD A401)'
        },
        data: null
      });
    }
    if(error.status === 500) {
      return response.status(error.status).json({
        message: {
          title: 'Error Interno',
          text: 'Por favor intente más tarde.\r(COD A500)'
        },
        data: null
      });
    }
    return response.status(error.status).json({
      message: {
        title: error.name,
        text: error.message
      },
      data: null
    });
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler
