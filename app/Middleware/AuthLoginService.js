'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const GE = require('@adonisjs/generic-exceptions')

class AuthLoginService {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request }, next) {
    const { authorization } = request.headers()
    const compare = `Basic ${Buffer.from('login@amipass:4m1p4ss.c0m').toString('base64')}`
    if(authorization != compare) {
      if(!`${authorization}`.startsWith('Bearer ')){
        throw new GE.HttpException(`Only login services user authorized or jwt can access the route`, 403)
      }
    }
    await next()
  }
}

module.exports = AuthLoginService
