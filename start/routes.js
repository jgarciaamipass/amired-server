'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

//WEB

Route.group(() => {
  Route.get('/', 'Web/LoginController.index');
  Route.get('/reset_password', 'Web/LoginController.resetPassword');
  Route.get('/reset/expired', 'Web/LoginController.resetPasswordExpired').as('resetExpired');
});

//API

//Home
Route.group(() => {
  Route.post('/', 'HomeController.index');
}).prefix('api/v2').middleware('auth');

//User
Route.group(() => {
  Route.post('/', 'UserController.login');
  Route.post('/signin', 'UserController.signin');
  Route.post('/signout', 'UserController.signout');
  Route.post('/logout', 'UserController.logout');
  Route.post('/refresh', 'UserController.refresh')
  Route.post('/reset/password', 'UserController.resetPassword');

  Route.get('/reset/password/mail_forgot', 'MailController.mailForgot');
  // Route.post('/checkExpiredToken', 'UserController.checkExpiredToken').as('validTokenWeb')
  //Route.get('/reset/password', 'MailController.index');
}).prefix('api/v2/login').middleware('guest');

Route.post('/checkExpiredToken', 'UserController.checkExpiredToken').prefix('api/v2/login').middleware(['auth:jwt']).as('validTokenWeb')

Route.post('/check', 'UserController.check').prefix('api/v2/login').middleware('auth');

Route.post('/change/password', 'UserController.changePassword').prefix('api/v2/login')
  .middleware(['authMac:session,auth:jwt']).as('changePassword');

Route.post('/change/password2', 'UserController.changePassword2').prefix('api/v2/login')
  .middleware('auth').as('changePassword2');
// Route.post('/checkExpiredToken', 'UserController.checkExpiredToken').prefix('api/v2/login')
//   .middleware('auth').as('validTokenWeb');
Route.post('/change/keypay', 'UserController.changeKeyPay').prefix('api/v2/login').middleware('auth');
Route.post('/info', 'UserController.deviceInfo').prefix('api/v2/deivce').middleware('auth');

Route.group(() => {
  Route.post('/show', 'UserConfigController.show')
  Route.post('/changeEnvironment', 'UserConfigController.changeEnvironment')
}).prefix('api/v2/user/configurations').middleware('auth');

//User Tags
Route.group(() => {
  Route.get('/', 'UserTagController.index');
  Route.get('/create', 'UserTagController.create');
  Route.post('/store', 'UserTagController.store');
  Route.get('/show/:id', 'UserTagController.show');
  Route.get('/edit/:id', 'UserTagController.edit');
  Route.put('/update/:id', 'UserTagController.update');
  Route.delete('/destroy/:id', 'UserTagController.destroy');
}).prefix('api/v2/user/tags').middleware('auth');

//Commerces
Route.group(() => {
  Route.post('/', 'CommerceController.index');
  Route.get('/create', 'CommerceController.create');
  Route.post('/store', 'CommerceController.store');
  Route.get('/show/:id', 'CommerceController.show');
  Route.get('/edit/:id', 'CommerceController.edit');
  Route.put('/update/:id', 'CommerceController.update');
  Route.delete('/destroy/:id', 'CommerceController.destroy');
  Route.get('/getTags', 'CommerceController.getTags');
  Route.post('/commerceCoverage', 'CommerceController.commerceCoverage');
}).prefix('api/v2/commerce').middleware('auth');

//Amibox
Route.group(() => {
  Route.post('/', 'CommerceController.withAmibox');
}).prefix('api/v2/commerce/amibox').middleware('auth');

//Commerce Store
Route.group(() => {
  Route.get('/', 'CommerceStoreController.index');
  Route.get('/create', 'CommerceStoreController.create');
  Route.post('/store', 'CommerceStoreController.store');
  Route.get('/show/:commerce/:store', 'CommerceStoreController.show');
  Route.get('/showuserpay/:code', 'CommerceStoreController.showuserpay');
  Route.get('/edit/:id', 'CommerceStoreController.edit');
  Route.put('/update', 'CommerceStoreController.update');
  Route.delete('/destroy/:id', 'CommerceStoreController.destroy');
  //Routes for MAC
  Route.put('/update/extends', 'CommerceStoreController.updateExtends')
}).prefix('api/v2/commerce/stores').middleware(['authMac:session,auth:jwt']);
Route.get('/uploadimage/:commerce/:store', 'CommerceStoreController.uploadImageAzure').prefix('api/v2/commerce/stores').middleware('guest');
Route.get('/showimage/:commerce/:store', 'CommerceStoreController.showImageAzure').prefix('api/v2/commerce/stores').middleware('guest');
Route.get('/listimage', 'CommerceStoreController.listImageAzure').prefix('api/v2/commerce/stores').middleware('guest');
Route.get('/uploadcover/:code', 'CommerceStoreController.uploadCover').prefix('api/v2/commerce/stores').middleware('guest');
Route.get('/migrate_images', 'CommerceStoreController.migrateImagesAzure').prefix('api/v2/commerce/stores').middleware('guest');

//Product
Route.group(() => {
  Route.post('/', 'ProductController.index');
  Route.post('/list', 'ProductController.list');
  Route.get('/create', 'ProductController.create');
  Route.post('/store', 'ProductController.store');
  Route.get('/show/:code', 'ProductController.show');
  Route.get('/show/instore/:commerce/:store', 'ProductController.showinstore');
  Route.get('/edit/:id', 'ProductController.edit');
  Route.put('/update/:id', 'ProductController.update');
  Route.delete('/destroy/:id', 'ProductController.destroy');
  //Amibox
  Route.get('/show/amibox/instore/:commerce/:store', 'ProductController.showAmiboxInStore');
  Route.post('/list', 'ProductController.listAmibox');
}).prefix('api/v2/commerce/product').middleware('auth');
Route.get('/uploadimage/:commerce/:store', 'ProductController.uploadImageAzure').prefix('api/v2/commerce/product').middleware('guest');

//Statement Account
Route.group(() => {
  Route.get('/', 'StatementAccountController.index');
  Route.get('/create', 'StatementAccountController.create');
  Route.post('/store', 'StatementAccountController.store');
  Route.post('/show/:username', 'StatementAccountController.show');
  Route.get('/edit/:id', 'StatementAccountController.edit');
  Route.put('/update/:id', 'StatementAccountController.update');
  Route.delete('/destroy/:id', 'StatementAccountController.destroy');
  Route.post('/balance', 'StatementAccountController.balance');
}).prefix('api/v2/statemens_account').middleware('auth');

//Cards
Route.group(() => {
  Route.get('/', 'CardController.index');
  Route.get('/create', 'CardController.create');
  Route.post('/store', 'CardController.store');
  Route.get('/show/:id', 'CardController.show');
  Route.get('/edit/:id', 'CardController.edit');
  Route.put('/update/:id', 'CardController.update');
  Route.post('/block', 'CardController.block');
  Route.post('/reactive', 'CardController.reactive');
  Route.delete('/destroy/:id', 'CardController.destroy');
}).prefix('api/v2/cards').middleware('auth');

//Employer
Route.group(() => {
  Route.get('/', 'EmployerController.index');
  Route.get('/create', 'EmployerController.create');
  Route.post('/store', 'EmployerController.store');
  Route.get('/show/:id', 'EmployerController.show');
  Route.get('/edit/:id', 'EmployerController.edit');
  Route.put('/update/:id', 'EmployerController.update');
  Route.delete('/destroy/:id', 'EmployerController.destroy');
}).prefix('api/v2/employer').middleware('auth');

//Employer Users
Route.group(() => {
  Route.get('/', 'EmployerUserController.index');
  Route.get('/create', 'EmployerUserController.create');
  Route.post('/store', 'EmployerUserController.store');
  Route.get('/show/:employer/:employee', 'EmployerUserController.show');
  Route.get('/edit/:id', 'EmployerUserController.edit');
  Route.put('/update/:id', 'EmployerUserController.update');
  Route.delete('/destroy/:id', 'EmployerUserController.destroy');
}).prefix('api/v2/employer/user').middleware('auth');

//Tags
Route.group(() => {
  Route.get('/', 'TagController.index');
  Route.get('/create', 'TagController.create');
  Route.post('/store', 'TagController.store');
  Route.get('/show/:id', 'TagController.show');
  Route.get('/edit/:id', 'TagController.edit');
  Route.put('/update/:id', 'TagController.update');
  Route.delete('/destroy/:id', 'TagController.destroy');
}).prefix('api/v2/tags').middleware('auth');

//Profile
Route.group(() => {
  Route.get('/', 'ProfileController.index');
  Route.get('/create', 'ProfileController.create');
  Route.post('/store', 'ProfileController.store');
  Route.get('/show/:id', 'ProfileController.show');
  Route.get('/edit/:id', 'ProfileController.edit');
  Route.put('/update', 'ProfileController.update');
  Route.delete('/destroy/:id', 'ProfileController.destroy');
}).prefix('api/v2/user/profile').middleware('auth');

//Transactions by Pay Smart Code
Route.group(() => {
  Route.post('/', 'PaySmartCodeController.index');
  Route.get('/create', 'PaySmartCodeController.create');
  Route.post('/store', 'PaySmartCodeController.store');
  Route.get('/show/:id', 'PaySmartCodeController.show');
  Route.get('/edit/:id', 'PaySmartCodeController.edit');
  Route.put('/update/:id', 'PaySmartCodeController.update');
  Route.delete('/destroy/:id', 'PaySmartCodeController.destroy');
}).prefix('api/v2/transactions/paysmart/code').middleware('auth');

//Transactions by Pay Smart QR
Route.group(() => {
  Route.get('/', 'PaySmartQrController.index');
  Route.get('/create', 'PaySmartQrController.create');
  Route.post('/store', 'PaySmartQrController.store');
  Route.get('/show/:id', 'PaySmartQrController.show');
  Route.get('/edit/:id', 'PaySmartQrController.edit');
  Route.put('/update/:id', 'PaySmartQrController.update');
  Route.delete('/destroy/:id', 'PaySmartQrController.destroy');
}).prefix('api/v2/transactions/paysmart/qr').middleware('auth');

//Transactions by Dynamic Key
Route.group(() => {
  Route.get('/', 'PaySmartDynamicKeyController.index');
  Route.get('/create', 'PaySmartDynamicKeyController.create');
  Route.post('/store', 'PaySmartDynamicKeyController.store');
  Route.get('/show/:id', 'PaySmartDynamicKeyController.show');
  Route.get('/edit/:id', 'PaySmartDynamicKeyController.edit');
  Route.put('/update/:id', 'PaySmartDynamicKeyController.update');
  Route.delete('/destroy/:id', 'PaySmartDynamicKeyController.destroy');
}).prefix('api/v2/transactions/paysmart/dynamickey').middleware('auth');

//Transfer
Route.group(() => {
  Route.post('/', 'TransferController.index');
  Route.get('/create', 'TransferController.create');
  Route.post('/store', 'TransferController.store');
  Route.get('/show/:id', 'TransferController.show');
  Route.get('/edit/:id', 'TransferController.edit');
  Route.put('/update/:id', 'TransferController.update');
  Route.delete('/destroy/:id', 'TransferController.destroy');
}).prefix('api/v2/transactions/transfers').middleware('auth');

//User transfer favorites
Route.group(() => {
  Route.post('/', 'TransferFavoriteAccountController.index');
  Route.get('/create', 'TransferFavoriteAccountController.create');
  Route.post('/store', 'TransferFavoriteAccountController.store');
  Route.get('/show/:id', 'TransferFavoriteAccountController.show');
  Route.get('/edit/:id', 'TransferFavoriteAccountController.edit');
  Route.put('/update/:id', 'TransferFavoriteAccountController.update');
  Route.delete('/destroy/:id', 'TransferFavoriteAccountController.destroy');
}).prefix('api/v2/user/transfers/favorites').middleware('auth');

Route.group(() => {
  Route.get('/issue/show', 'ReportIssueController.show');
  Route.post('/issue/store', 'ReportIssueController.store');
}).prefix('api/v2/configurations/report').middleware('auth');

//Mail 
Route.group(() => {
  Route.post('/issue', 'MailController.reportIssue');
}).prefix('api/v2/configurations/report').middleware('auth');

//Communicate
Route.group(() => {
  Route.get('/', 'CommunicationController.index');
  Route.get('/create', 'CommunicationController.create');
  Route.post('/store', 'CommunicationController.store');
  Route.get('/show/:employer/:branch', 'CommunicationController.show');
  Route.get('/edit/:id', 'CommunicationController.edit');
  Route.put('/update/:id', 'CommunicationController.update');
  Route.delete('/destroy/:id', 'CommunicationController.destroy');
  Route.get('/published', 'CommunicationController.published');
  Route.post('/seen', 'CommunicationController.seen');
}).prefix('api/v2/notifications/communicate').middleware('auth');

//Wallet Types
Route.group(() => {
  Route.get('/', 'WalletTypeController.index');
  Route.get('/create', 'WalletTypeController.create');
  Route.post('/store', 'WalletTypeController.store');
  Route.get('/show/:id', 'WalletTypeController.show');
  Route.get('/edit/:id', 'WalletTypeController.edit');
  Route.put('/update/:id', 'WalletTypeController.update');
  Route.delete('/destroy/:id', 'WalletTypeController.destroy');
}).prefix('api/v2/configurations/wallet/type').middleware('auth');

Route.group(() => {
  Route.get('/', 'WalletController.index');
  Route.get('/create', 'WalletController.create');
  Route.post('/store', 'WalletController.store');
  Route.get('/show/:id', 'WalletController.show');
  Route.get('/edit/:id', 'WalletController.edit');
  Route.put('/update/:id', 'WalletController.update');
  Route.post('/change_state', 'WalletController.changeState');
}).prefix('api/v2/wallet').middleware('auth');

Route.group(() => {
  Route.get('/', 'TermsConditionController.index');
  Route.get('/create', 'TermsConditionController.create');
  Route.post('/store', 'TermsConditionController.store');
  Route.get('/show/:id', 'TermsConditionController.show');
  Route.get('/edit/:id', 'TermsConditionController.edit');
  Route.put('/update/:id', 'TermsConditionController.update');
  Route.delete('/delete/:id', 'TermsConditionController.delete');
}).prefix('api/v2/terms').middleware('auth');

Route.get('/last_terms', 'TermsConditionController.last_terms').prefix('api/v2/terms').middleware('guest');

Route.group(() => {
  Route.get('/', 'UserTermsController.index');
  Route.get('/last', 'UserTermsController.last');
  Route.get('/create', 'UserTermsController.create');
  Route.post('/store', 'UserTermsController.store');
  Route.get('/show/:id', 'UserTermsController.show');
  Route.get('/edit/:id', 'UserTermsController.edit');
  Route.put('/update/:id', 'UserTermsController.update');
  Route.delete('/delete/:id', 'UserTermsController.delete');
}).prefix('api/v2/user/profile/terms').middleware('auth');

Route.group(() => {
  Route.get('/', 'VersionAppController.index');
  Route.get('/create', 'VersionAppController.create');
  Route.post('/store', 'VersionAppController.store');
  Route.get('/show/:id', 'VersionAppController.show');
  Route.get('/edit/:id', 'VersionAppController.edit');
  Route.put('/update/:id', 'VersionAppController.update');
  Route.delete('/delete/:id', 'VersionAppController.delete');
}).prefix('api/v2/version/app').middleware('auth');