'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CardTransactionsSchema extends Schema {
  up () {
    this.create('card_transactions', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('card_transactions')
  }
}

module.exports = CardTransactionsSchema
