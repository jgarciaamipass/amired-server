'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserTransferSchema extends Schema {
  up () {
    this.create('user_transfers', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('user_transfers')
  }
}

module.exports = UserTransferSchema
