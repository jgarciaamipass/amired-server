'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserTagsSchema extends Schema {
  up () {
    this.create('user_tags', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('user_tags')
  }
}

module.exports = UserTagsSchema
