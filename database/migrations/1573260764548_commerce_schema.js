'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CommerceSchema extends Schema {
  up () {
    this.create('commerce', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('commerce')
  }
}

module.exports = CommerceSchema
