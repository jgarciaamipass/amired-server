'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AspnetUsersSchema extends Schema {
  up () {
    this.create('aspnet_users', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('aspnet_users')
  }
}

module.exports = AspnetUsersSchema
