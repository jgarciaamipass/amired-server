'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmployerUserSchema extends Schema {
  up () {
    this.create('employer_users', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('employer_users')
  }
}

module.exports = EmployerUserSchema
