'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CommerceStoreSchema extends Schema {
  up () {
    this.create('commerce_stores', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('commerce_stores')
  }
}

module.exports = CommerceStoreSchema
