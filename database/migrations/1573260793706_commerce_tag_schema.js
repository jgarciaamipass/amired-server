'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CommerceTagSchema extends Schema {
  up () {
    this.create('commerce_tags', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('commerce_tags')
  }
}

module.exports = CommerceTagSchema
