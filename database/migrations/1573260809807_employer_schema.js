'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmployerSchema extends Schema {
  up () {
    this.create('employers', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('employers')
  }
}

module.exports = EmployerSchema
